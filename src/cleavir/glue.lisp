(in-package #:cleavir-clisp)

;;;; This file defines the user interface between Cleavir and CLISP.

(defparameter *use-cleavir* nil "Switches to cleavir for compilation.")

(setf (fdefinition 'clisp-compile-lambdabody) #'sys::compile-lambdabody)
(setf (fdefinition 'clisp-compile) #'compile)
(setf (fdefinition 'clisp-eval) #'eval)
(setf (fdefinition 'clisp-compile-toplevel-form) #'sys::compile-toplevel-form)
(setf (fdefinition 'clisp-compile-lambda) #'sys::compile-lambda)

(defun compile-lambdabody (name lambdabody)
  (if *use-cleavir*
      (let ((*use-cleavir* nil))
        (cleavir-compile `(lambda ,@lambdabody) name))
      (clisp-compile-lambdabody name lambdabody)))

(compile 'compile-lambdabody)

(defun %compile (name &optional definition)
  (let ((cleavir-generate-ast::*compiler* 'cl:compile))
    (clisp-compile name definition)))

(compile '%compile)

(defun %eval (form)
  (let ((cleavir-generate-ast::*compiler* 'cl:eval))
    (clisp-eval form)
    #+nil
    (if *use-cleavir*
        (funcall (compile-lambdabody nil `(() ,form)))
        (clisp-eval form))))

(compile '%eval)

(defun %compile-toplevel-form (form &optional (sys::*toplevel-name* sys::*toplevel-name*))
  (let ((cleavir-generate-ast::*compiler* 'cl:compile-file))
    (clisp-compile-toplevel-form form sys::*toplevel-name*)))

(compile '%compile-toplevel-form)

(defun %compile-lambda (name lambdabody %venv% %fenv% %benv% %genv% %denv%
                       error-when-failed-p)
  (let ((*use-cleavir* nil))
    (clisp-compile-lambda name lambdabody %venv% %fenv% %benv% %genv% %denv%
                          error-when-failed-p)))

(compile '%compile-lambda)

(progn
  (setf (fdefinition 'sys::compile-lambdabody) #'compile-lambdabody)
  (setf (fdefinition 'cl:compile) #'%compile)
  ;; for this to do anything we must actually replace the funtab entty
  #+(or)
  (setf (fdefinition 'cl:eval) #'%eval)
  (setf (fdefinition 'sys::compile-toplevel-form) #'%compile-toplevel-form)
  (setf (fdefinition 'sys::compile-lambda) #'%compile-lambda))
