(cl:in-package #:cleavir-clisp-ir)

;;;; We define additional HIR instructions specific to CLISP in this
;;;; file.

(defclass system-funcall-instruction (cleavir-ir:instruction cleavir-ir:one-successor-mixin cleavir-ir:side-effect-mixin)
  ((%function-name :initarg :function-name :accessor function-name)))

(defmethod cleavir-ir-graphviz:label ((instruction system-funcall-instruction))
  (format nil "system funcall ~a" (function-name instruction)))

(defun make-system-funcall-instruction (function-name inputs outputs &optional (successor nil successor-p))
  (make-instance 'system-funcall-instruction
                 :function-name function-name
                 :inputs inputs
                 :outputs outputs
                 :successors (if successor-p (list successor) nil)))

(defmethod cleavir-ir:clone-initargs append ((instruction system-funcall-instruction))
  (list :function-name (function-name instruction)))

;;; This instruction is used to implement phi instructions. Think of
;;; it as an explicit store/assignment in the predecessor branches of
;;; a phi instruction for the purpose of loading that predecessor
;;; branch specific value of phi..
(defclass unphi-instruction (cleavir-ir:instruction cleavir-ir:one-successor-mixin) ())

(defmethod cleavir-ir-graphviz:label ((instruction unphi-instruction)) "unphi")

(defclass catch-close-instruction (cleavir-ir:instruction cleavir-ir:one-successor-mixin cleavir-ir:side-effect-mixin) ())

(defmethod cleavir-ir-graphviz:label ((instruction catch-close-instruction)) "catch close")

(defclass push-instruction (cleavir-ir:instruction cleavir-ir:one-successor-mixin cleavir-ir:side-effect-mixin) ())

(defmethod cleavir-ir-graphviz:label ((instruction push-instruction)) "push")

;;; The first input of this instruction is the static link to the
;;; containing closure vector. The rest are CELLs.
(defclass make-vector1-instruction (cleavir-ir:instruction cleavir-ir:one-successor-mixin) ())

(defmethod cleavir-ir-graphviz:label ((instruction make-vector1-instruction)) "make-vector1")
