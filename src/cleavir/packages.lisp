(cl:in-package #:common-lisp-user)

(defpackage #:cleavir-clisp
  (:use #:common-lisp)
  (:export #:*use-cleavir*))

(defpackage #:cleavir-clisp-ir
  (:use #:common-lisp)
  (:export #:system-funcall-instruction
           #:function-name
           #:convert-ssa-form
           #:edge-split-graph
           #:ssa-dead-code-elimination
           #:split-return-instructions
           #:unphi-instruction
           #:implement-phi-functions
           #:count-basic-block-phi-functions
           #:insert-catch-close
           #:ssa-copy-propagation
           #:ssa-constant-propagation
           #:ssa-loop-hoist
           #:hoist-cells
           #:delete-superfluous-branches
           #:aggressive-dead-phi-elimination))

(defpackage #:cleavir-clisp-ast
  (:use #:common-lisp)
  (:export #:system-funcall-ast
           #:system-funcall-ast-function-name))

(defpackage #:cleavir-clisp-primop
  (:use #:common-lisp)
  (:export #:system-funcall))
