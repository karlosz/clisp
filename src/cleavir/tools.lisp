(in-package #:cleavir-clisp)

(defmacro with-toplevel-fnode-setup ((definition) &body body)
  `(flet ((closure-slot (obj num)
            (if (sys::closurep obj)
                (sys::%record-ref obj num)
                nil)))
     (let ((system::*fenv* (closure-slot ,definition 5))
           (system::*benv* (closure-slot ,definition 6))
           (system::*genv* (closure-slot ,definition 7))
           (system::*venv* (closure-slot ,definition 4))
           (system::*venvc* nil)
           (systeM::*denv* (or (closure-slot ,definition 8)
                               system::*toplevel-denv*)))
       ,@body)))

(defun check-ir-consistency (enter-instruction)
  (let ((succn (make-hash-table))
        (predn (make-hash-table)))
    (cleavir-ir:map-instructions
     (lambda (instruction)
       (setf (gethash instruction succn) (length (cleavir-ir:successors instruction)))
       (setf (gethash instruction predn) (length (cleavir-ir:predecessors instruction))))
     enter-instruction)
    (cleavir-ir:map-instructions
     (lambda (instruction)
       (dolist (predecessor (cleavir-ir:predecessors instruction))
         (assert (member instruction (cleavir-ir:successors predecessor)))
         (decf (gethash predecessor succn)))
       (dolist (successor (cleavir-ir:successors instruction))
         (assert (member instruction (cleavir-ir:predecessors successor)))
         (decf (gethash successor predn))))
     enter-instruction)
    (maphash (lambda (inst n)
               (assert (zerop n)))
             succn)
    (maphash (lambda (inst n)
               (assert (zerop n)))
             predn)))

(defun clisp-hir-transformations (enter-instruction)
  (cleavir-partial-inlining:do-inlining enter-instruction)
  (cleavir-clisp-ir:delete-superfluous-branches enter-instruction)
  (cleavir-hir-transformations:process-captured-variables enter-instruction)
  (cleavir-clisp-ir:hoist-cells enter-instruction)
  ;; needs to happen after clousre variables are processed
  (cleavir-hir-transformations:eliminate-catches enter-instruction)
  (cleavir-ir:set-predecessors enter-instruction)
  ;; SSA in Cleavir at the moment is very fragile. The relationship
  ;; between predecessors and inputs must be maintained throughout,
  ;; until phi nodes are fully implemented. That means not calling
  ;; set-predecessors, etc. which can shuffle the order of
  ;; predecessors. Also watch out for inputs being shuffled on phi
  ;; instructions.
  (cleavir-clisp-ir:convert-ssa-form enter-instruction)
  (cleavir-clisp-ir:ssa-dead-code-elimination enter-instruction)
  (cleavir-clisp-ir:aggressive-dead-phi-elimination enter-instruction)
  ;; Aggressive dead code elimination triggers more dead code
  ;; elimination
  (cleavir-clisp-ir:ssa-dead-code-elimination enter-instruction)
  ;; translation REQUIRES copy propogation
  (cleavir-clisp-ir:ssa-copy-propagation enter-instruction)
  (cleavir-clisp-ir:edge-split-graph enter-instruction)
  (cleavir-clisp-ir:ssa-constant-propagation enter-instruction)
  ;; we need to dead code eliminate again because now some funcalls
  ;; are easier to analyze
  (cleavir-clisp-ir:ssa-dead-code-elimination enter-instruction)
  ;; needs to happen before phi is implemented but after any code is
  ;; eliminated
  (cleavir-clisp-ir:edge-split-graph enter-instruction)
  ;; We need to do constant propagation before loop hoisting to make
  ;; sure that we can hoist as much as possible/
  (cleavir-clisp-ir:ssa-loop-hoist enter-instruction)
  (cleavir-clisp-ir:implement-phi-functions enter-instruction)
  (cleavir-clisp-ir:insert-catch-close enter-instruction)
  (cleavir-clisp-ir:split-return-instructions enter-instruction)
  (cleavir-ir:set-predecessors enter-instruction))

(defun graph-form (form &key (name "viz") (optimize-fun #'clisp-hir-transformations enter-instruction) (kill-nop t))
  (let* ((cleavir-generate-ast::*compiler* 'compile)
         (enter-instruction (compile-cleavir form)))
    (when optimize-fun
      (funcall optimize-fun enter-instruction))
    (when kill-nop
      (mapc #'cleavir-ir:delete-instruction (cleavir-ir:instructions-of-type enter-instruction 'cleavir-ir:nop-instruction)))
    (when name
      (cleavir-ir-graphviz:draw-flowchart enter-instruction
                                          (format nil "/tmp/~a" name))
      (uiop:run-program (format nil "dot -Tpdf /tmp/~a -o /tmp/~a.pdf" name name))
      (uiop:run-program (format nil "evince /tmp/~a.pdf" name)))
    enter-instruction))

(defun graph-initial (inst)
  (cleavir-ir-graphviz:draw-flowchart inst "/tmp/viz")
  (uiop:run-program "dot -Tpdf /tmp/viz -o /tmp/viz.pdf")
  (uiop:run-program "evince /tmp/viz.pdf"))

(defun compile-cleavir (form)
  (let ((*macroexpand-hook* #'cleavir-macroexpand))
    (cleavir-ast-to-hir:compile-toplevel-unhoisted
     #-generate-ast
     (cleavir-cst-to-ast:cst-to-ast
      (cst:cst-from-expression form)
      (make-instance 'clisp-environment)
      (make-instance 'clisp-system))
     #+generate-ast
     (cleavir-generate-ast:generate-ast
      form
      (make-instance 'clisp-environment)
      (make-instance 'clisp-system)))))

(defun print-asm (listing &optional abbreviatep)
  (dolist (insts (if abbreviatep
                     (system::insert-combined-LAPs listing)
                     listing))
    (format t "~&~a" insts)))

(defun cleavir-lap (form)
  (with-toplevel-fnode-setup (form)
    (let ((enter (compile-cleavir form)))
      (clisp-hir-transformations enter)
      (translate enter))))

(defun preprocess-fnode (fnode lambda-list keyword-offset)
  (with-accessors ((reqvar cleavir-code-utilities:required)
                   (optvar cleavir-code-utilities:optionals)
                   (keys cleavir-code-utilities:keys)
                   (restvar cleavir-code-utilities:rest-body)
                   (allow-other-keys cleavir-code-utilities:allow-other-keys))
      (cleavir-code-utilities:parse-ordinary-lambda-list lambda-list)
    (setf (sys::fnode-req-num fnode) (length reqvar)
          (sys::fnode-opt-num fnode) (if (eq optvar :none) 0 (length optvar))
          (sys::fnode-rest-flag fnode) (not (eql restvar :none))
          ;; TODO
          (sys::fnode-keyword-flag fnode) (not (eq keys :none)) ;keyflag
          (sys::fnode-keywords fnode) (if (eq keys :none)
                                          nil
                                          (mapcar #'caar keys))
          (sys::fnode-lambda-list fnode) lambda-list
          (sys::fnode-allow-other-keys-flag fnode) allow-other-keys))
  ; actually parse this stuff?
  #+nil
  (multiple-value-bind (body-rest declarations docstring)
      ;; fixme parse lambda form better
      (system::parse-body (cdr (rest (rest definition))) t)
    (setf (sys::fnode-documentation fnode) docstring)
    ;; FIXME parse type declarations
    (setq declarations declarations))
  (setf (sys::fnode-venvconst fnode)
        (not (and (null (sys::fnode-far-used-vars fnode))
                  (null (sys::fnode-far-assigned-vars fnode)))))
  (setf (sys::fnode-Consts-Offset fnode)
        (+ (setf (sys::fnode-Keyword-Offset fnode)
                 keyword-offset
                 #+(or)
                 (+ (setf (sys::fnode-Tagbodys-Offset fnode)
                          (+ (setf (sys::fnode-Blocks-Offset fnode)
                                   (if (sys::fnode-venvconst fnode) 1 0))
                             (length (sys::fnode-Blocks fnode))))
                    (length (sys::fnode-Tagbodys fnode))))
           (length (sys::fnode-Keywords fnode))))
  (setf (sys::fnode-code fnode)
        (sys::make-anode
         :type 'LAMBDABODY
                                        ; :source lambdabody
                                        ; :sub-anodes anode-list
         :seclass sys::*seclass-foldable*
                                        ; :stackz oldstackz
                                        ;:code codelist
         )))

(defun compile-form (definition &optional name)
  (with-toplevel-fnode-setup (definition)
    (let ((fnode (system::make-fnode :enclosing nil
                                     :venvc nil
                                     :name name)))
      (multiple-value-bind (code-list consts ltv-forms keyword-offset)
          (cleavir-lap definition)
        (preprocess-fnode fnode (second definition) keyword-offset)
        (setf (sys::fnode-consts fnode) consts)
        ;; Actually investigate load time value handling
        (setf (sys::fnode-consts-forms fnode) ltv-forms)
        (let ((SPdepth (sys::SP-depth code-list)))
          ;; determine stack requirements
          (setq code-list (sys::insert-combined-LAPs code-list))
          (sys::create-fun-obj fnode (sys::assemble-LAP code-list) SPdepth)))
      (values (sys::fnode-code fnode)
              ;fnode
              ))))

(defun cleavir-compile (form &optional name)
  (let ((cleavir-generate-ast::*compiler*
          (if (boundp 'cleavir-generate-ast::*compiler*)
              cleavir-generate-ast::*compiler*
              'cl:compile))
        (*use-cleavir* nil))
    (handler-bind ((cleavir-env:no-function-info
                     (lambda (condition)
                       (warn "Undefined function ~a, treating as global." (cleavir-env:name condition))
                       (invoke-restart 'cleavir-cst-to-ast:consider-global))))
      (compile-form form name))))

(defun disassemble-all (fun)
  (let ((count 0))
    (labels ((aux (fun)
               (disassemble fun)
               (dolist (const (ignore-errors (sys::closure-consts fun)))
                 (when (compiled-function-p const)
                   (aux const)
                   (incf count)))))
      (aux fun)
      count)))
