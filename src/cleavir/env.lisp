(in-package #:cleavir-clisp)

;;;; Environment methods to hook Cleavir's environment into CLISP's
;;;; environment query tools.
(defclass clisp-system () ())

(defun make-clisp-macroexpand-environment ()
  (make-array 2))

(defclass clisp-environment ()
  ((%env :reader clisp-%env :initform (make-clisp-macroexpand-environment))))

(defgeneric cleavir->host (environment))

(defmethod cleavir->host (environment)
  ;; assume we're at the global
  ;; (and that the global is a host environment)
  environment)

;; Investigate why there are so many copies of the same binding
(defun env-augment-thing (env name thing kind)
  (let ((index (ecase kind (:variable 0) (:functoid 1))))
    (setf (aref (clisp-%env env) index)
          (vector name thing (aref (clisp-%env env) index))))
  env)

#-cleavir
(compile 'env-augment-thing)

(defmethod cleavir->host ((environment cleavir-env:lexical-variable))
  (env-augment-thing (cleavir->host (cleavir-env::next environment))
                     (cleavir-env:name environment)
                     :lexical-variable
                     :variable))

(defmethod cleavir->host ((environment cleavir-env:special-variable))
  (env-augment-thing (cleavir->host (cleavir-env::next environment))
                     (cleavir-env:name environment)
                     ;; CLISP marks special variables as #<SPECIAL
                     ;; REFERENCE>, but that seems to be marked in C.
                     :special-variable
                     :variable))

(defmethod cleavir->host ((environment cleavir-env:symbol-macro))
  (env-augment-thing (cleavir->host (cleavir-env::next environment))
                     (cleavir-env:name environment)
                     (system::make-symbol-macro (cleavir-env:expansion environment))
                     :variable))

(defmethod cleavir->host ((environment cleavir-env:function))
  (env-augment-thing (cleavir->host (cleavir-env::next environment))
                     (cleavir-env:name environment)
                     `#',(cleavir-env:name environment)
                     :functoid))

(defmethod cleavir->host ((environment cleavir-env:macro))
  (env-augment-thing (cleavir->host (cleavir-env::next environment))
                     (cleavir-env:name environment)
                     (system::make-macro (cleavir-env:expander environment)
                                         nil)
                     :functoid))

(defmethod cleavir->host ((environment cleavir-env::entry))
  (cleavir->host (cleavir-env::next environment)))

(defun cleavir-macroexpand (expander form env)
  (if (typep env 'array)
      (funcall expander form env)
      (funcall expander form (clisp-%env (cleavir->host env)))))

(defmethod cleavir-env:eval (form environment1 (environment2 clisp-environment))
  (system::eval-env form (eval '(ext:the-environment))))

(defun make-system-funcall-compiler-macro (function)
  ;; make a compiler macro toto translate system funcalls we use this
  ;; instead of returning and inlining asts always because cleavir
  ;; doesn't yet support wiring the rest/optional arguments to an
  ;; inlined node directly yet
  (lambda (form env)
    (declare (cons form)) (declare (ignore env))
    (let ((args (rest form)))
      (list* 'cleavir-clisp-primop:system-funcall function args))))

(defmacro defspecial (name lambda-list &body body)
  (let ((head (gensym "HEAD"))
        (form (gensym "FORM"))
        (environment (gensym "ENVIRONMENT"))
        (system (gensym "SYSTEM")))
    `(defmethod cleavir-generate-ast:convert-special
         ((,head (eql ',name)) ,form ,environment (,system clisp-system))
       (cleavir-generate-ast:convert
        (destructuring-bind ,lambda-list
            (rest ,form)
          ,@body)
        ,environment ,system))))

(defmacro define-cleavir-macro (name lambda-list &body body)
  `(defmethod cleavir-env:function-info ((environment clisp-environment) (symbol (eql ',name)))
     (make-instance 'cleavir-env:global-macro-info
                    :compiler-macro nil
                    :name ',name
                    :expander (lambda (form env)
		                (declare (ignore env))
		                (destructuring-bind ,lambda-list (rest form)
                                  ,@body)))))

(defmethod cleavir-env:variable-info ((environment clisp-environment) symbol)
  (cond (;; We can check whether this symbol names a constant variable
	 ;; by checking the return value of CONSTANTP. 
	 (constantp symbol)
	 ;; If it is a constant variable, we can get its value by
	 ;; calling SYMBOL-VALUE on it.
	 (make-instance 'cleavir-env:constant-variable-info
	   :name symbol
	   :value (symbol-value symbol)))
	(;; If it is not a constant variable, we can check whether
	 ;; macroexpanding it alters it.
	 (not (eq symbol (macroexpand-1 symbol)))
	 ;; Clearly, the symbol is defined as a symbol macro.
	 (make-instance 'cleavir-env:symbol-macro-info
	   :name symbol
	   :expansion (macroexpand-1 symbol)))
	(;; If it is neither a constant variable nor a symbol macro,
	 ;; it might be a special variable.  We can start by checking
	 ;; whether it is bound.
	 (boundp symbol)
	 ;; It may or may not be special.  It is not special if it has
	 ;; not been proclaimed as such but it has been set using
	 ;; (SETF SYMBOL-VALUE).  We have no way of checking that, so
	 ;; we assume that it has been proclaimed special, since that
	 ;; is probably the most common case.
	 (make-instance 'cleavir-env:special-variable-info
	   :name symbol
	   :global-p t))
        #+(or)
	(;; If it is not bound, it could still be special.  If so, it
	 ;; might have a restricted type on it.  It will then likely
	 ;; fail to bind it to an object of some type that we
	 ;; introduced, say our bogus environment.  It is not fool
	 ;; proof because it could have the type STANDARD-OBJECT.  But
	 ;; in the worst case, we will just fail to recognize it as a
	 ;; special variable.
	 (null (ignore-errors
                 (eval `(let ((,symbol (make-instance 'clisp-environment])))
                          t))))
	 ;; It is a special variable.  However, we don't know its
	 ;; type, so we assume it is T, which is the default.
	 (make-instance 'cleavir-env:special-variable-info
                        :name symbol
                        :global-p t))
        ;; If the previous test fails, it could still be special
        ;; without any type restriction on it.  We can try to
        ;; determine whether this is the case by checking whether the
        ;; ordinary binding (using LET) of it is the same as the
        ;; dynamic binding of it.  This method might fail because the
        ;; type of the variable may be restricted to something we
        ;; don't know and that we didn't catch before, 
        ((ignore-errors
           (eval `(let ((,symbol 'a))
                    (progv '(,symbol) '(b) (eq ,symbol (symbol-value ',symbol))))))
	 ;; It is a special variable.  However, we don't know its
	 ;; type, so we assume it is T, which is the default.
         (make-instance 'cleavir-env:special-variable-info
                        :name symbol
                        :global-p t))
	(;; Otherwise, this symbol does not have any variable
	 ;; information associated with it.
	 t
	 ;; Return NIL as the protocol stipulates.
	 nil)))

(defun compiler-primitive-p (name)
  (and (or (eq (symbol-package name)
               (find-package :cleavir-primop))
           (eq (symbol-package name)
               (find-package :cleavir-clisp-primop)))
       ;; we punt on these until we can define methods
       ;; on these primops to generate bytecode instead
       (not (eq name 'cleavir-primop:call-with-variable-bound))
       (not (eq name 'cleavir-primop:multiple-value-call))))

(defmethod cleavir-env:function-info ((environment clisp-environment) function-name)
  (cond (;; If the function name is the name of a macro, then
	 ;; MACRO-FUNCTION returns something other than NIL.
	 (and (symbolp function-name) (not (null (macro-function function-name))))
	 ;; If so, we know it is a global macro.  It is also safe to
	 ;; call COMPILER-MACRO-FUNCTION, because it returns NIL if
	 ;; there is no compiler macro associated with this function
	 ;; name.
	 (make-instance 'cleavir-env:global-macro-info
	   :name function-name
	   :expander (macro-function function-name)
	   :compiler-macro (compiler-macro-function function-name)))
	(;; If it is not the name of a macro, it might be the name of
	 ;; a special operator.  This can be checked by calling
	 ;; special-operator-p.
	 (and (symbolp function-name)
              (or (special-operator-p function-name)
                  (compiler-primitive-p function-name)))
	 (make-instance 'cleavir-env:special-operator-info
                        :name function-name))
        (;; If it is a CLISP built-in function, we always inline a
         ;; system call AST
         (gethash function-name system::function-codes)
         (make-instance 'cleavir-env:global-function-info
                        :name function-name
                        :compiler-macro (make-system-funcall-compiler-macro function-name)))
	(;; If it is neither the name of a macro nor the name of a
	 ;; special operator, it might be the name of a global
	 ;; function.  We can check this by calling FBOUNDP.  Now,
	 ;; FBOUNDP returns true if it is the name of a macro or a
	 ;; special operator as well, but we have already checked for
	 ;; those cases.
	 (fboundp function-name)
	 ;; In that case, we return the relevant info
	 (make-instance 'cleavir-env:global-function-info
	   :name function-name
	   :compiler-macro (compiler-macro-function function-name)))
	(;; If it is neither of the cases above, then this name does
	 ;; not have any function-info associated with it.
	 t
	 ;; Return NIL as the protocol stipulates.
	 nil)))

(defmethod cleavir-env:function-info ((env clisp-environment) (sym (eql 'cl:catch)))
  (make-instance 'cleavir-env:global-macro-info
    :compiler-macro nil
    :name 'cl:catch
    :expander (lambda (form env)
		(declare (ignore env))
		(destructuring-bind (tag &body body)
		    (rest form)
		  `(%catch ,tag (lambda () ,@body))))))

(defmethod cleavir-env:function-info ((env clisp-environment) (sym (eql 'cl:throw)))
  (make-instance 'cleavir-env:global-macro-info
    :compiler-macro nil
    :name 'cl:throw
    :expander (lambda (form env)
		(declare (ignore env))
		(destructuring-bind (tag result)
		    (rest form)
		  `(multiple-value-call #'%throw ,tag ,result)))))

(defmethod cleavir-env:function-info ((env clisp-environment) (sym (eql 'cl:progv)))
  (make-instance 'cleavir-env:global-macro-info
    :compiler-macro nil
    :name 'cl:progv
    :expander (lambda (form env)
		(declare (ignore env))
		(destructuring-bind (vars vals &body body)
		    (rest form)
		  `(%progv ,vars ,vals (lambda () ,@body))))))

(defmethod cleavir-env:function-info ((env clisp-environment) (sym (eql 'cl:unwind-protect)))
  (make-instance 'cleavir-env:global-macro-info
    :compiler-macro nil
    :name 'cl:unwind-protect
    :expander (lambda (form env)
		(declare (ignore env))
		(destructuring-bind (protected &body cleanup)
		    (rest form)
		  `(%unwind-protect (lambda () ,protected) (lambda () ,@cleanup))))))

(defmethod cleavir-env:function-info ((env clisp-environment) (sym (eql 'cl:values)))
  (make-instance 'cleavir-env:global-function-info
                 :name 'cl:values
                 :compiler-macro
                 (lambda (form env)
                   (declare (ignore env))
                   (destructuring-bind (&rest arguments)
                       (rest form)
                     `(cleavir-primop:values ,@arguments)))))

(defmethod cleavir-env:function-info ((env clisp-environment) (sym (eql 'cl:funcall)))
  (make-instance 'cleavir-env:global-function-info
                  :name 'cl:funcall
                  :compiler-macro
                  (lambda (form env)
                    (declare (ignore env))
                    (destructuring-bind (function &rest arguments)
                        (rest form)
                      ;; The funcall bytecode instruction handles
                      ;; coercion to the correct type
                      `(cleavir-primop:funcall ,function ,@arguments)))))

;; These should be defined with the CLISP compiler
#-cleavir
(progn
  (defun %unwind-protect (protected-thunk cleanup-thunk)
    (unwind-protect (funcall protected-thunk) (funcall cleanup-thunk)))

  (defun %catch (tag body-thunk)
    (catch tag (funcall body-thunk)))

  (defun %throw (tag &rest values)
    (throw tag (values-list values)))

  (defun %progv (vars vals body-thunk)
    (progv vars vals (funcall body-thunk)))

  (compile '%unwind-protect)
  (compile '%catch)
  (compile '%throw)
  (compile '%progv))

(defun %handler-nop (x) (declare (ignore x)))

(defun %handler-bind (body-thunk &rest clauses)
  (if clauses
      ;; use CLISP's compile only for handler bind, not the actual thunks
      (let ((*use-cleavir* nil))
        (funcall
         (compile nil
                  `(lambda ()
                     (system::%handler-bind
                      ,body-thunk
                      ,@(let (args)
                          (loop until (null clauses)
                                do (push `',(pop clauses) args)
                                   (push `(lambda (condition)
                                            ;; Don't ask me why we
                                            ;; need a NOP here. Maybe
                                            ;; because otherwise the
                                            ;; funcall would get
                                            ;; optimized away?
                                            (%handler-nop (funcall ,(pop clauses)
                                                                   condition)))
                                         args))
                          (nreverse args)))))))
      (funcall body-thunk)))

(defmethod cleavir-env:function-info ((env clisp-environment) (sym (eql 'system::%handler-bind)))
  (make-instance 'cleavir-env:global-macro-info
    :compiler-macro nil
    :name 'system::%handler-bind
    :expander (lambda (form env)
		(declare (ignore env))
		(destructuring-bind (body-thunk &rest clauses) (rest form)
                  `(%handler-bind
                    ,body-thunk
                    ,@clauses)))))

;; Overriding CLISP's non-ANSI behavior
(defmethod cleavir-generate-ast:check-special-form-syntax ((head (eql 'cl:function)) form)
  (cleavir-code-utilities:check-form-proper-list form)
  (cleavir-code-utilities:check-argcount form 1 2)
  (cond ((cleavir-generate-ast::proper-function-name-p (second form))
	 nil)
	((consp (second form))
	 (unless (eq (first (second form)) 'lambda)
	   (error 'cleavir-generate-ast::function-argument-must-be-function-name-or-lambda-expression
		  :expr (second form)))
	 (unless (cleavir-code-utilities:proper-list-p (second form))
	   (error 'cleavir-generate-ast::lambda-must-be-proper-list
		  :expr (second form))))
	(t
	 (error 'cleavir-generate-ast::function-argument-must-be-function-name-or-lambda-expression
		:expr (cadr form)))))

(defmethod cleavir-generate-ast:convert-special ((symbol (eql 'cl:function)) form env (system clisp-system))
  (cleavir-generate-ast::db s (function name . tail) form
    (declare (ignore function))
    (if (and (null tail)
             (cleavir-generate-ast::proper-function-name-p name))
	(cleavir-generate-ast::convert-named-function name env system)
	(cleavir-generate-ast::convert-lambda-function
         (if (null tail)
             name
             (first tail))
         env system))))

(defmethod cleavir-cst-to-ast:convert-special ((symbol (eql 'cl:function)) cst env (system clisp-system))
  #+(or)
  (check-function-syntax cst)
  (cst:db origin (function-cst name-cst . tail-cst) cst
    (declare (ignore function-cst))
    (let ((result (if (and (cst:null tail-cst)
                           (cleavir-cst-to-ast::proper-function-name-p name-cst))
                      (cleavir-cst-to-ast::convert-named-function name-cst env system)
                      (cleavir-cst-to-ast::convert-lambda-function
                       (if (cst:null tail-cst)
                           name-cst
                           (cst:first tail-cst)) env system))))
      (reinitialize-instance result :origin origin))))

;; COMPILE-FILE doesn't seem to respect this anyway. Bring this back when we write our own.
#+(or)
(defmethod cleavir-env:function-info ((env clisp-environment) (sym (eql 'defun)))
  (make-instance 'cleavir-env:global-macro-info
    :compiler-macro nil
    :name 'cl:defun
    :expander (lambda (form env)
		(declare (ignore env))
		(destructuring-bind (name lambda-list &body body)
                    (rest form)
                  `(let nil
                     (system::remove-old-definitions ',name)
                     (eval-when (:compile-toplevel)
                       (system::c-defun ',name (system::lambda-list-to-signature ',lambda-list)))
                     (system::%putd ',name (labels ((,name ,lambda-list
                                                      (declare (system::in-defun ',name))
                                                      (block ,name ,@body)))
                                             #',name))
                     ;; do this until we have macroexpansion
                     (funcall #'(setf sys::closure-name)
                              ',name
                              (fdefinition ',name))
                     (eval-when (:load-toplevel :execute)
                       (system::%put ',name 'system::definition
                                     (cons ',form (ext:the-environment))))
                     ',name)))))

(defmethod cleavir-env:function-info ((env clisp-environment) (sym (eql 'ext:the-environment)))
  (make-instance 'cleavir-env:global-macro-info
                 :compiler-macro nil
                 :name 'ext:the-environment
                 :expander (lambda (form env)
                             (declare (ignore form env))
                             `(progn
                                (eval-when (:compile-toplevel :load-toplevel)
                                  (system::%uncompilable 'ext:the-environment))
                                (let ((custom:*evalhook* #'system::%the-environment))
                                  0)))))

(defun %generic-function-lambda (lambda-list outer-decl declarations docstring body-thunk)
  (let ((*use-cleavir* nil))
    (funcall (compile nil `(lambda ()
                             ;; TODO fill in nil
                             (let ()
                               ,outer-decl
                               (system::%generic-function-lambda
                                ,lambda-list
                                (declare ,@declarations)
                                ,docstring
                                (funcall ,body-thunk
                                         ,@(make-lambda-list-callable
                                            lambda-list)))))))))

(defun make-lambda-list-callable (lambda-list)
  (let* ((parsed (cleavir-code-utilities:parse-ordinary-lambda-list lambda-list))
         (required (cleavir-code-utilities:required parsed))
         (keys (cleavir-code-utilities:keys parsed))
         (rest (cleavir-code-utilities:rest-body parsed))
         (optional (cleavir-code-utilities:optionals parsed)))
    (flet ((coerce-to-nil (x)
             (if (eq x :none)
                 nil
                 x)))
      (append (coerce-to-nil required)
              (remove nil (mapcar #'first (coerce-to-nil optional)))
              (remove nil (mapcan (lambda (k-spec)
                                    (destructuring-bind ((keyword var) default)
                                        k-spec
                                      (declare (ignore default))
                                      (list keyword var)))
                                  (coerce-to-nil keys)))
              (remove nil (list (coerce-to-nil rest)))))))

(defmethod cleavir-env:function-info ((env clisp-environment) (sym (eql 'system::%generic-function-lambda)))
  (make-instance 'cleavir-env:global-macro-info
                 :name 'system::%generic-function-lambda
                 :expander
                 (lambda (form env)
                   (destructuring-bind (lambda-list &body body)
                       (rest form)
                     ;; hack based on its callers
                     (multiple-value-bind (body-rest declarations docstring)
                         (system::parse-body body)
                       (let ((outer-decl (if (member '(declare (inline funcall)
                                                       (ignore args))
                                                     body
                                                     :test #'equal)
                                             '`(DECLARE ,@(compile-no-jitc 'uninitialized-prototype-factory))
                                             (if (member '(inline funcall) declarations :test #'equal)
                                                 '`(DECLARE ,@(compile-no-jitc (sys::closure-name gf) 'prototype-factory))
                                                 '`(DECLARE ,@(safe-gf-declspecs gf)
                                                            ,@(compile-no-jitc (sys::closure-name gf)
                                                                               'preliminary))))))
                         `(%generic-function-lambda
                           ',lambda-list
                           outer-decl
                           ',declarations
                           ,docstring
                           (lambda ,(make-lambda-list-callable lambda-list)
                             ,@body))))))))

(defmethod cleavir-env:function-info ((env clisp-environment) (sym (eql 'system::function-macro-let)))
  (make-instance 'cleavir-env:global-macro-info
                 :name 'system::function-macro-let
                 :expander
                 (lambda (form env)
                   (declare (ignore env))
                   (destructuring-bind ((&rest defs) body)
                       (rest form)
                     `(labels ,(mapcar (lambda (def)
                                         (destructuring-bind (name (lambda-list &body body) . tail)
                                             def
                                           (declare (ignore tail))
                                           `(,name ,lambda-list ,@body)))
                                defs)
                        ,body)))))

(defmethod cleavir-env:optimize-info ((env clisp-environment))
  (let* ((compilation-speed 1)
	 (safety 1)
	 (space 1)
	 (debug 1)
	 (speed 1)
	 (optimize `((compilation-speed ,compilation-speed)
		     (safety ,safety)
		     (space ,space)
		     (debug ,debug)
		     (speed ,speed)))
	 (policy (cleavir-policy:compute-policy optimize env)))
    (make-instance 'cleavir-env:optimize-info
		   :optimize optimize
		   :policy policy)))

(defmethod cleavir-env:declarations
    ((environment clisp-environment))
  '(SYSTEM::IN-DEFUN SYSTEM::READ-ONLY SYSTEM::IMPLEMENTATION-DEPENDENT))

(defmethod cleavir-env:type-expand ((environment clisp-environment) type)
  (ext:type-expand type))

(defun function-foldable-p (function)
  (sys::seclass-foldable-p (sys::function-side-effect function)))

(defun function-side-effects-p (function)
  (sys::seclass-modifies (sys::function-side-effect function)))

(defmethod cleavir-env:has-extended-char-p ((environment clisp-environment))
  t)

(defmethod cleavir-env:float-types ((environment clisp-environment))
  '(short-float single-float double-float long-float))

(defmethod cleavir-env:upgraded-complex-part-types ((environment clisp-environment))
  '(real))

(defmethod cleavir-env:upgraded-array-element-types ((environment clisp-environment))
  ;; TODO
  nil)
