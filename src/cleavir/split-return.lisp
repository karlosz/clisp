(in-package #:cleavir-clisp-ir)

;;;; We split returns to avoid values locations being split between
;;;; branches and merging into a single RET instruction.

(defun split-return-instructions (enter-instruction)
  (dolist (return (cleavir-ir:instructions-of-type enter-instruction 'cleavir-ir:return-instruction))
    (dolist (predecessor (cleavir-ir:predecessors return))
      (let ((cleavir-ir:*policy* (cleavir-ir:policy predecessor))
            (cleavir-ir:*dynamic-environment* (cleavir-ir:dynamic-environment predecessor)))
        (let ((new-return (cleavir-ir:make-return-instruction (cleavir-ir:inputs return))))
          (cleavir-ir:insert-instruction-between
           new-return
           predecessor
           return)
          (setf (cleavir-ir:successors new-return) nil)))))
  (cleavir-ir:reinitialize-data enter-instruction))
