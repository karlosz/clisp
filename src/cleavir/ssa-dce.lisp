(in-package #:cleavir-clisp-ir)

;;;; Removing useless instructions in the SSA graph acts as a dead
;;;; code remover. NOTE: should be superseded by a worklist version of
;;;; remove-useless-instructions.

(defgeneric may-eliminate (instruction))

(defmethod may-eliminate (instruction) t)

(defmethod may-eliminate ((instruction cleavir-ir:side-effect-mixin)) nil)

(defmethod may-eliminate ((instruction cleavir-ir:multiple-successors-mixin)) nil)

(defmethod may-eliminate ((instruction cleavir-ir:enter-instruction)) nil)

(defmethod may-eliminate ((instruction cleavir-clisp-ir:system-funcall-instruction))
  (not (cleavir-clisp::function-side-effects-p (function-name instruction))))

(defmethod may-eliminate ((instruction cleavir-ir:funcall-instruction))
  (let ((input (first (cleavir-ir:inputs instruction))))
    (when (constant-input-p input)
      (not (cleavir-clisp::function-side-effects-p (constant-input-value input))))))

(defmethod may-eliminate ((instruction cleavir-ir:multiple-to-fixed-instruction))
  (dolist (output (cleavir-ir:outputs instruction) t)
    (when (cleavir-ir:using-instructions output)
      (return nil))))

(defun ssa-dead-code-elimination (enter-instruction)
  (let ((worklist (data-of-type enter-instruction '(or cleavir-ir:lexical-location
                                                    cleavir-ir:values-location))))
    (loop (when (null worklist) (return))
          (let* ((v (pop worklist))
                 (using-instructions (cleavir-ir:using-instructions v)))
            ;; FIXME: M->F with no outputs do not get deleted. Figure
            ;; out how to fit it in with this algorithm.
            (when (or (null using-instructions)
                      (and (null (rest using-instructions))
                           ;; v's only use is to define itself. i.e this is a phi node
                           (member v (cleavir-ir:outputs (first using-instructions)))))
              (let ((instruction (first (cleavir-ir:defining-instructions v))))
                (when (and instruction (may-eliminate instruction))
                  (dolist (x (cleavir-ir:inputs instruction))
                    (pushnew x worklist)
                    (setf (cleavir-ir:using-instructions x)
                          (delete instruction
                                  (cleavir-ir:using-instructions x))))
                  (if (typep instruction 'cleavir-ir:phi-instruction)
                      (delete-phi instruction)
                      (change-class instruction 'cleavir-ir:nop-instruction
                                    :inputs '()
                                    :outputs '())))))))))
