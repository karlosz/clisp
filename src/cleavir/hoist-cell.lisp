(in-package #:cleavir-clisp-ir)

;;;; Hoisting cells to the toplevel of their containing loop.

;;;; This pass has a few benefits:

;;;; 1. We allocate fewer closure vectors.

;;;; 2. We eliminate cases where cells are stuck in the strange
;;;; structure of catch-unwind branches.

;;;; Correctness is ensured because we don't hoist cells past the
;;;; loops that contain them. this way we guarantee that the amount of
;;;; dynamic cells created is the same.

(defun hoist-cells (initial-instruction)
  (let ((cells '()))
    (cleavir-ir:map-instructions-with-owner
     (lambda (instruction owner)
       (when (typep instruction 'cleavir-ir:create-cell-instruction)
         (push (cons instruction owner) cells)))
     initial-instruction)
    (when (null cells)
      (return-from hoist-cells))
    (let ((owners (remove-duplicates (mapcar #'cdr cells))))
      (dolist (owner owners)
        (let* ((loops (compute-loops owner #'cleavir-ir:successors))
               (cells-with-owner (mapcar #'car
                                         (remove-if-not (lambda (cell)
                                                          (eq owner (cdr cell)))
                                                        cells)))
               (table (cells-in-loops loops)))
          (dolist (cell cells-with-owner)
            (multiple-value-bind (loop seen)
                (gethash cell table)
              (if seen
                  (hoist-cell-to cell (header-node loop))
                  (hoist-cell-to cell owner)))))))
    (cleavir-ir:reinitialize-data initial-instruction)
    (cleavir-ir:set-predecessors initial-instruction)))

(defun hoist-cell-to (cell instruction)
  (let ((inputs (cleavir-ir:inputs cell))
        (outputs (cleavir-ir:outputs cell)))
    (unless (eq cell instruction)
      (cleavir-ir:delete-instruction cell)
      (cleavir-ir:insert-instruction-after cell instruction)
      (setf (cleavir-ir:inputs cell) inputs)
      (setf (cleavir-ir:outputs cell) outputs))))

;;; Find the cells in each loop, but we associate cells only with
;;; their innermost loop. We do this by looking up the length of the
;;; loop.
(defun cells-in-loops (loops)
  (let ((table (make-hash-table :test #'eq)))
    (dolist (loop loops)
      (dolist (node (nodes loop))
        (when (typep node 'cleavir-ir:create-cell-instruction)
          (multiple-value-bind (other-loop seen)
              (gethash node table)
            (if seen
                ;; This means LOOP is nested in OTHER-LOOP
                (when (< (size loop) (size other-loop))
                  (setf (gethash node table) loop))
                (setf (gethash node table) loop))))))
    table))
