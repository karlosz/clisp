(in-package #:cleavir-clisp)

;;;; A basic block scheduler to remove unnecessary JMPs. The algorithm
;;;; currently doesn't take loops into account at all.

;; For efficiency reasons, we store lap-code in reverse order
(defclass lap-basic-block (cleavir-basic-blocks:basic-block)
  ((%lap-code :reader lap-code :initarg :lap-code)
   (%marked :accessor marked :initform nil)))

(defun annotate-basic-block (lap-code basic-block)
  (change-class basic-block 'lap-basic-block
                :lap-code lap-code))

;; Generate a trace
(defun generate-trace (annotated-block)
  (let (trace)
    ;; We make the assumption that each branching block ends with a
    ;; JMP to its last instruction's first successor.
    (do ((annotated-block (if (marked annotated-block)
                              nil
                              annotated-block)
                          (find-if-not #'marked
                                       (cleavir-basic-blocks:successors annotated-block)
                                       :from-end t))
         (last nil annotated-block))
        ((null annotated-block)
         (when last
           (push (lap-code last) trace)))
      ;; Skip the JMP if we can.
      (when last
        (push (if (eq (first (cleavir-ir:successors
                              (cleavir-basic-blocks:last-instruction last)))
                      (cleavir-basic-blocks:first-instruction annotated-block))
                  (rest (lap-code last))
                  (lap-code last))
              trace))
      (setf (marked annotated-block) t))
    (nreverse (reduce #'nconc trace :from-end t))))

;;; Use a very simple tracing algorithm to linearize basic blocks.
(defun trace-schedule-blocks (annotated-blocks)
  (loop for annotated-block in annotated-blocks
        append (generate-trace annotated-block)))
