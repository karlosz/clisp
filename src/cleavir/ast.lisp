(cl:in-package #:cleavir-clisp-ast)

;;;; We define CLISP specific ASTs in this file.

(defclass system-funcall-ast (cleavir-ast:ast)
  ((%argument-asts :initarg :argument-asts :reader cleavir-ast:argument-asts)
   (%function-name :initarg :function-name :reader system-funcall-ast-function-name)))

(defmethod cleavir-ast:children ((ast system-funcall-ast))
  (cleavir-ast:argument-asts ast))

(cleavir-io:define-save-info system-funcall-ast
  (:argument-asts cleavir-ast:argument-asts)
  (:function-name cleavir-clisp-ast:system-funcall-ast-function-name))
