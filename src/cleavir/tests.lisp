(cleavir-compile '(lambda (n) (dotimes (i n) (print i))))
(cleavir-compile '(lambda (x) (if (= x 1) (progn (let ((z x)) (print z) (setq z 1))) x)))
(cleavir-compile '(lambda (x) (1+ (if (= x 1) (progn (let ((z x)) (print z) (setq z 1))) x))))
(cleavir-compile '(lambda (x z) (cons (lambda (y) (+ y z))
                            (lambda (w) (lambda (y)
                                     (+ x y w))))))
(cleavir-compile '(lambda (x y) (+ x y)))
(cleavir-compile '(lambda ()
                   (labels ((fact (n)
                              (if (zerop n)
                                  1
                                  (* n (fact (1- n))))))
                     #'fact)))
(cleavir-compile '(lambda (num)
                   (let ((z 0)
                         (p1 1)
                         (p2 1))
                     (dotimes (i (- num 2))
                       (setf z (+ p1 p2)
                             p2 p1
                             p1 z))
                     z)))
(cleavir-compile '(lambda ()
                   (let ((x nil))
                     (values
                      (case 2
                        (1 (setq x 'a) 'w)
                        (2 (setq x 'b) 'y)
                        (t (setq x 'c) 'z))
                      x))))
(cleavir-compile '(lambda () (block done
                          (flet ((%f (x) (return-from done x)))
                            (%f 'good))
                          'bad)))
(cleavir-compile '(lambda ()
                   (let ((x 0))
                     (tagbody
                        (flet ((%f (y) (setq x y) (go a)))
                          (%f 10))
                        (setq x 1)
                      a)
                     x)))
(cleavir-compile '(lambda ()
                   (let ((x 0))
                     (block a
                       (flet ((%f (y) (setq x y) (return-from a y)))
                         (%f 10))
                       (setq x 1))
                     x)))
(cleavir-compile '(lambda ()
                   (let ((x 0))
                     (block a
                       (flet ((%f (y)
                                (flet ((%g (z)
                                         (return-from a (+ x y z))))
                                  (%g 10))
                                'bad))
                         (%f 14)
                         'badder)
                       'super-bad))))
(cleavir-compile '(lambda ()
                   (block %f
                     (flet ((%f (&key (x (return-from %f :good)))
                              nil))
                       (%f)
                       :bad))))
(cleavir-compile '(lambda ()
                   (let ((z 4))
                     (block %f
                       (flet ((%f (&key (x (progn
                                             (print z)
                                             (return-from %f :good))))
                                nil))
                         (%f)
                         :bad)))))
(cleavir-compile '(lambda ()
                   (let ((z 4))
                     (block %f
                       (flet ((%f (&key x)
                                z))
                         (print (%f))
                         :bad)))))
(cleavir-compile '(lambda ()
                   (flet ((%f (&key a (b 0 b-p)) (values a b (not (not b-p)))))
                     (%f))))
(cleavir-compile '(lambda () (let ((i 0) a b c d)
                          (values
                           (and (setq a (setq i (1+ i)))
                                (setq b (setq i (1+ i)))
                                (setq c (setq i (1+ i)))
                                (setq d (setq i (1+ i))))
                           i a b c d))))
(cleavir-compile '(lambda ()
                   (let ((i 0) x y)
                     (values
                      (nth-value (progn (setf x (incf i)) 3)
                                 (progn (setf y (incf i)) (values 'a 'b 'c 'd 'e 'f 'g)))
                      i x y))))
(cleavir-compile '(lambda ()
                   (let ((x nil))
                     (ignore-errors
                      (unwind-protect
                           (progn (push 1 x) (error "Boo!"))
                        (incf (car x))))
                     x)))
(cleavir-compile '(lambda ()
                   (let* ((x (list 'a 'b 'c 'd 'e))
                          (y (list 'f 'g 'h 'i 'j))
                          (p1 1) (p2 2) (len 3)
                          (z '(10 11 12)))
                     (rotatef (subseq x p1 (+ p1 len))
                              (subseq y p2 (+ p2 len))
                              z)
                     (values x y z))))
(cleavir-compile '(lambda ()
                   (dotimes (i 5)
                     (dotimes (j 5)
                       (print i)
                       (print j)))))
(cleavir-compile '(lambda (ftype-ll arg-asts)
  (block nil
    (let ((state :req)
          (list-229550 ftype-ll)
          (param nil))
      (tagbody system::begin-loop (when (endp list-229550)
                                    (go system::end-loop))
         (setq param (car list-229550))
         (if (find param '(&rest))
             (setf state param)
             (if (eq state :req)
                 (if arg-asts
                     (return 'bad)
                     (setf arg-asts arg-asts))
                 (if (eq state '&rest)
                     (return (mapcar (lambda (a) (list param a)) arg-asts))
                     nil)))
         (psetq list-229550 (cdr list-229550))
         (go system::begin-loop)
       system::end-loop
         (return 'bad))))))
(cleavir-compile '(lambda ()
                   (let* ((a1 (make-array 5 :initial-contents '(a b c d e)))
                          (a2 (adjust-array a1 4 :displaced-to nil)))
                     (assert (if (adjustable-array-p a1)
                                 (eq a1 a2)
                                 (equal (array-dimensions a1) '(5))))
                     (assert (not (array-displacement a2)))
                     a2)))
(cleavir-compile '(lambda (symbol)
                   (if (ignore-errors
                        (eval `(let ((,symbol 'a))
                                 (progv '(,symbol) '(b) (eq ,symbol (symbol-value ',symbol))))))
                       t
                       nil)))
(cleavir-compile '(lambda ()
                   (block bar
                     (block foo
                       (unwind-protect
                            (return-from bar)
                         (return-from foo)))
                     1)))
(cleavir-compile '(lambda ()
                   (block a (block b
                              (setf *f* (lambda () (return-from b)))
                              (return-from a)
                              (print 1)))))
(cleavir-compile '(lambda ()
                   (let ((x 0) (y 0))
                     (multiple-value-prog1 (values x y)
                       (incf x) (incf y 2)))))
;; test with values-type = (values &rest t)
(cleavir-compile '(lambda (values-type)
                   (let ((original-values-type values-type))
                     (setf values-type (rest values-type))
                     (values
                      nil
                      (when (eq (car values-type) '&optional)
                        (pop values-type)
                        (loop while (and (consp values-type)
                                         (not (eq (car values-type) '&rest)))
                              collect (pop values-type)))
                      (when (eq (car values-type) '&rest)
                        (unless (null (cddr values-type))
                          (error 'values-&rest-syntax :expr original-values-type))
                        (second values-type))
                      (eq (car values-type) '&rest)))))
(cleavir-compile '(lambda (symbol)
                   (block restart-381311
                     (let ((g382061
                             (lambda nil
                               (return-from restart-381311
                                 (error 'cleavir-environment:no-variable-info :name symbol)))))
                       (system::%handler-bind g382061 'condition
                                              (lambda (condition)
                                                (signal condition)))))))
(cleavir-compile '(lambda (x)
                   (block hey
                     (let ((result (= x 1)))
                       (loop while (null result)
                             do (block foo
                                  (tagbody
                                     (return-from foo)
                                   there
                                     (return-from foo
                                       (apply (lambda () (return-from hey)) nil)))))))))
(cleavir-compile '(lambda () (let ((f))
                          (block foo
                            (setf f (lambda () (return-from foo))))
                          (funcall f))))
;; test if this compiles
(cleavir-compile '(lambda (lambda-list &key (normalize t)
                       allow-specializers
                       (normalize-optional normalize)
                       (normalize-keyword normalize)
                       (normalize-auxilary normalize))
                    (let ((state :required)
                          (allow-other-keys nil)
                          (auxp nil)
                          (required nil)
                          (optional nil)
                          (rest nil)
                          (keys nil)
                          (keyp nil)
                          (aux nil))
                      (labels ((fail (elt)
                                 (simple-program-error "Misplaced ~S in ordinary lambda-list:~%  ~S"
                                                                      elt lambda-list))
                               (check-variable (elt what &optional (allow-specializers allow-specializers))
                                 (unless (and (or (symbolp elt)
                                                  (and allow-specializers
                                                       (consp elt) (= 2 (length elt)) (symbolp (first elt))))
                                              (not (constantp elt)))
                                   (simple-program-error "Invalid ~A ~S in ordinary lambda-list:~%  ~S"
                                                         what elt lambda-list)))
                               (check-spec (spec what)
                                 (destructuring-bind (init suppliedp) spec
                                   (declare (ignore init))
                                   (check-variable suppliedp what nil))))
                        (dolist (elt lambda-list)
                          (case elt
                            (&optional
                             (if (eq state :required)
                                 (setf state elt)
                                 (fail elt)))
                            (&rest
                             (if (member state '(:required &optional))
                                 (setf state elt)
                                 (fail elt)))
                            (&key
                             (if (member state '(:required &optional :after-rest))
                                 (setf state elt)
                                 (fail elt))
                             (setf keyp t))
                            (&allow-other-keys
                             (if (eq state '&key)
                                 (setf allow-other-keys t
                                       state elt)
                                 (fail elt)))
                            (&aux
                             (cond ((eq state '&rest)
                                    (fail elt))
                                   (auxp
                                    (simple-program-error "Multiple ~S in ordinary lambda-list:~%  ~S"
                                                          elt lambda-list))
                                   (t
                                    (setf auxp t
                                          state elt))
                                   ))
                            (otherwise
                             (when (member elt '#.(set-difference lambda-list-keywords
                                                                  '(&optional &rest &key &allow-other-keys &aux)))
                               (simple-program-error
                                "Bad lambda-list keyword ~S in ordinary lambda-list:~%  ~S"
                                elt lambda-list))
                             (case state
                               (:required
                                (check-variable elt "required parameter")
                                (push elt required))
                               (&optional
                                (cond ((consp elt)
                                       (destructuring-bind (name &rest tail) elt
                                         (check-variable name "optional parameter")
                                         (cond ((cdr tail)
                                                (check-spec tail "optional-supplied-p parameter"))
                                               ((and normalize-optional tail)
                                                (setf elt (append elt '(nil))))
                                               (normalize-optional
                                                (setf elt (append elt '(nil nil)))))))
                                      (t
                                       (check-variable elt "optional parameter")
                                       (when normalize-optional
                                         (setf elt (cons elt '(nil nil))))))
                                (push (ensure-list elt) optional))
                               (&rest
                                (check-variable elt "rest parameter")
                                (setf rest elt
                                      state :after-rest))
                               (&key
                                (cond ((consp elt)
                                       (destructuring-bind (var-or-kv &rest tail) elt
                                         (cond ((consp var-or-kv)
                                                (destructuring-bind (keyword var) var-or-kv
                                                  (unless (symbolp keyword)
                                                    (simple-program-error "Invalid keyword name ~S in ordinary ~
                                                         lambda-list:~%  ~S"
                                                                          keyword lambda-list))
                                                  (check-variable var "keyword parameter")))
                                               (t
                                                (check-variable var-or-kv "keyword parameter")
                                                (when normalize-keyword
                                                  (setf var-or-kv (list (make-keyword var-or-kv) var-or-kv)))))
                                         (cond ((cdr tail)
                                                (check-spec tail "keyword-supplied-p parameter"))
                                               ((and normalize-keyword tail)
                                                (setf tail (append tail '(nil))))
                                               (normalize-keyword
                                                (setf tail '(nil nil))))
                                         (setf elt (cons var-or-kv tail))))
                                      (t
                                       (check-variable elt "keyword parameter")
                                       (setf elt (if normalize-keyword
                                                     (list (list (make-keyword elt) elt) nil nil)
                                                     elt))))
                                (push elt keys))
                               (&aux
                                (if (consp elt)
                                    (destructuring-bind (var &optional init) elt
                                      (declare (ignore init))
                                      )
                                    (progn
                                      
                                      (setf elt (list* elt (when normalize-auxilary
                                                             '(nil))))))
                                (push elt aux))
                               (t))))))
                      (values (nreverse required) (nreverse optional) rest (nreverse keys)
                              allow-other-keys (nreverse aux) keyp))))
;; failing
(macrolet ((%m (&key a b c) `(quote (,a ,b ,c))))
    (values
     (%m :allow-other-keys nil)
     (%m :a 1 :allow-other-keys nil)
     (%m :allow-other-keys t)
     (%m :allow-other-keys t :allow-other-keys nil :foo t)
     (%m :allow-other-keys t :c 1 :b 2 :a 3)
     (%m :allow-other-keys nil :c 1 :b 2 :a 3)))
(progn
    (let ((foo-default 1)
          (bar-default 2))
      (defmacro defmacro.13-macro ((&key (foo foo-default foo-p)
                                         (bar bar-default bar-p)))
        `(list ',foo ,(notnot foo-p) ',bar ,(notnot bar-p))))
    (mapcar #'eval '((defmacro.13-macro nil)
                     (defmacro.13-macro (:foo x))
                     (defmacro.13-macro (:bar y))
                     (defmacro.13-macro (:foo nil :bar nil :foo 4 :bar 14))
                     (defmacro.13-macro (:foo 1 :bar 2))
                     (defmacro.13-macro (:foo x :bar y :foo z))
                     (defmacro.13-macro (:bar y :bar z :foo x)))))
