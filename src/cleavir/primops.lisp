(cl:in-package #:cleavir-clisp-primop)

(defmethod cleavir-generate-ast:convert-special ((symbol (eql 'cleavir-clisp-primop:system-funcall)) form environment (system cleavir-clisp::clisp-system))
  (check-type (second form) symbol)
  (make-instance 'cleavir-clisp-ast:system-funcall-ast
                 :function-name (second form)
                 :argument-asts (cleavir-generate-ast:convert-sequence (rest (rest form)) environment system)))

(defmethod cleavir-generate-ast::check-special-form-syntax ((head (eql 'cleavir-clisp-primop:system-funcall)) form)
  (system::proper-list-length-in-bounds-p form 1))

(defmethod cleavir-cst-to-ast:convert-special
    ((symbol (eql 'cleavir-clisp-primop:system-funcall)) cst env (system cleavir-clisp::clisp-system))
  (cleavir-cst-to-ast::check-cst-proper-list cst 'form-must-be-proper-list)
  (cleavir-cst-to-ast::check-argument-count cst 1 nil)
  (make-instance 'cleavir-clisp-ast:system-funcall-ast
                 :function-name (cst:raw (cst:second cst))
                 :argument-asts (cleavir-cst-to-ast::convert-sequence (cst:rest (cst:rest cst)) env system)
                 :origin (cst:source cst)))

(defun cleavir-primop:call-with-variable-bound (variable value thunk)
  (progv (list variable) (list value)
    (funcall thunk)))

(cleavir-clisp::defspecial multiple-value-call (function . forms)
  (case (length forms)
    (0 `(funcall ,function))
    (t `(cleavir-primop:multiple-value-call
            (coerce ,function 'function)
          ,@(mapcar (lambda (form) `(lambda () ,form)) forms)))))

(defmethod cleavir-cst-to-ast:convert-special
    ((symbol (eql 'multiple-value-call)) cst env (system cleavir-clisp::clisp-system))
  (cst:db origin (function-cst . form-csts) (cst:rest cst)
    (cleavir-cst-to-ast:convert
     (case (length (cst:raw form-csts))
       (0 (cleavir-ast:make-call-ast
           (cleavir-cst-to-ast:convert function-cst)
           nil))
       (t  (cst:cst-from-expression
           `(cleavir-primop:multiple-value-call
                (coerce ,(cst:raw function-cst) 'function)
              ,@ (mapcar (lambda (form) `(lambda () ,form))
                         (cst:raw form-csts))))))
     env system)))

#-cleavir
(defun cleavir-primop:multiple-value-call (function &rest args)
  (apply function (mapcan (lambda (arg)
                            (multiple-value-list (funcall arg)))
                          args)))

(cleavir-clisp::define-cleavir-macro multiple-value-bind (vars form &body body)
  (let ((syms (loop for var in vars collecting (gensym (symbol-name var)))))
    `(cleavir-primop:let-uninitialized (,@syms)
       (cleavir-primop:multiple-value-setq (,@syms) ,form)
       (let (,@(loop for var in vars
                     for sym in syms
                     collecting `(,var ,sym)))
         ,@body))))

(cleavir-clisp::define-cleavir-macro multiple-value-setq (vars form)
  (if vars
      `(sys::multiple-value-setf ,vars ,form)
      `(values ,form)))
