(in-package #:cleavir-clisp-ir)

;;;; We need to insert a CATCH-CLOSE instruction to correspond to
;;;; CLISP's BLOCK-CLOSE. Since arbitrary transformations can happen
;;;; which invalidate the syntax tree, we use a more flow graph
;;;; structured approach based on dominance frontiers.

;; CATCH-CLOSE insertion works on an edge-split graph
(defun insert-catch-close (initial-instruction)
  ;; catch close instructions must be added in order.
  (cleavir-ir:map-instructions-with-owner
   (lambda (catch-instruction owner)
     (when (typep catch-instruction 'cleavir-ir:catch-instruction)
       (let* ((basic-blocks (cleavir-basic-blocks:basic-blocks initial-instruction))
              (instruction-basic-blocks (cleavir-basic-blocks:instruction-basic-blocks basic-blocks))
              (dominance-frontiers (cleavir-dominance:dominance-frontiers
                                    (gethash owner instruction-basic-blocks)
                                    #'cleavir-basic-blocks:successors))
              (dominance-tree
                (cleavir-dominance:dominance-tree
                 (gethash owner instruction-basic-blocks) #'cleavir-basic-blocks:successors))
              (df (gethash (gethash (first (cleavir-ir:successors catch-instruction))
                                    instruction-basic-blocks)
                           dominance-frontiers)))
         ;; this should be exactly one basic block
         (when df
           (assert (= (length df) 1))
           (let* ((dominance-frontier (first df))
                  (catch-close
                    (make-instance 'catch-close-instruction
                                   :inputs (list (first (cleavir-ir:outputs catch-instruction)))
                                   :policy (cleavir-ir:policy catch-instruction)
                                   :dynamic-environment (cleavir-ir:dynamic-environment catch-instruction)))
                  (preds (remove-if-not
                          (lambda (pred)
                            (member (gethash (first (cleavir-ir:successors catch-instruction))
                                             instruction-basic-blocks)
                                    (cleavir-dominance:dominators dominance-tree (gethash pred instruction-basic-blocks))))
                          (cleavir-ir:predecessors (cleavir-basic-blocks:first-instruction dominance-frontier)))))
             (assert preds)
             ;; pick the predecessors dominated by the first
             ;; successor of catch
             (dolist (pred preds)
               (setf (cleavir-ir:successors pred)
                     (substitute catch-close
                                 (cleavir-basic-blocks:first-instruction dominance-frontier)
                                 (cleavir-ir:successors pred)))
               (setf (cleavir-ir:successors catch-close)
                     (list (cleavir-basic-blocks:first-instruction dominance-frontier))))
             (setf (cleavir-ir:predecessors catch-close)
                   preds))))))
   initial-instruction))
