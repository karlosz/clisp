This is an alternative compiler to CLISP's built-in compiler, which
relies on Cleavir, an independent project located at:
https://github.com/robert-strandh/SICL

One can load this compiler into a live CLISP image through (load "load.lisp") in this directory.

Then setting the dynamic variable cleavir-clisp:*use-cleavir* will redirect COMPILE and COMPILE-FILE to use this compiler instead.

The compiler runs much faster when compiled, rather than interpreted. To self-host the compiler:

(setf cleavir-clisp:*use-cleavir* t)
(push :cleavir *features*)
(let ((sys::*load-compiling* t)) (load "load.lisp"))
(let ((sys::*load-compiling* t)) (load "load.lisp"))

For details about how the Cleavir side of the compiler works, check out the documentation in the SICL repository located under Code/Cleavir/Documentation which should explain most of the terminology and framework built upon by these files.