(in-package #:cleavir-clisp-ir)

(defun delete-superfluous-branches (initial-instruction)
  (dolist (eq (cleavir-ir:instructions-of-type initial-instruction 'cleavir-ir:eq-instruction))
    (when (eq (first (cleavir-ir:successors eq))
              (second (cleavir-ir:successors eq)))
      (cleavir-ir:bypass-instruction (first (cleavir-ir:successors eq))
                                     eq))))
