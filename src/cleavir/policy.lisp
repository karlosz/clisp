(in-package :cleavir-clisp)

(defmethod cleavir-policy:compute-policy-quality
    ((quality (eql 'cleavir-kildall-type-inference:insert-type-checks))
     optimize
     (environment clisp-environment))
  t
  ;; One possible optimize strategy
  #+(or)
  (> (cleavir-policy:optimize-value optimize 'speed)
     (cleavir-policy:optimize-value optimize 'compilation-speed)))
