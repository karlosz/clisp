(in-package #:cleavir-clisp-ir)

;;;; SSA constant propagation uses flow sensitive information in the
;;;; SSA graph to derive unreachable instructions and places where
;;;; constants can be propagated.

(defgeneric constant-foldable (instruction args))

(defmethod constant-foldable (instruction args) nil)

(defmethod constant-foldable ((instruction cleavir-ir:funcall-instruction) args)
  (cleavir-clisp::function-foldable-p (first args)))

(defmethod constant-foldable ((instruction cleavir-clisp-ir:system-funcall-instruction) args)
  (cleavir-clisp::function-foldable-p (function-name instruction)))

(defmethod constant-foldable ((instruction cleavir-ir:assignment-instruction) args) t)

(defmethod constant-foldable ((instruction cleavir-ir:multiple-to-fixed-instruction) args)
  (null (rest (cleavir-ir:outputs instruction))))

(defmethod constant-foldable ((instruction cleavir-ir:fdefinition-instruction) args)
  (and (symbolp (first args))
       (eq (symbol-package (first args))
           (find-package "COMMON-LISP"))
       (eq (nth-value 1 (find-symbol (symbol-name (first args))
                                     "COMMON-LISP"))
           :external)
       (compiled-function-p (fdefinition (first args)))))

;;; Given some arguments, constant fold.
(defgeneric constant-fold (instruction args))

(defmethod constant-fold (instruction args) (error "not implemented"))

(defun function-fold (instruction function args)
  (let ((values (multiple-value-list (apply function args)))
        (output (first (cleavir-ir:outputs instruction))))
    (cond ((and (null (rest values))
                (or (null (rest (cleavir-ir:using-instructions output)))
                    (typep (first (cleavir-ir:using-instructions output))
                           'cleavir-ir:multiple-to-fixed-instruction)))
           (first values))
          (t
           (let ((new-output (cleavir-ir:make-values-location))
                 (cleavir-ir:*policy* (cleavir-ir:policy instruction))
                 (cleavir-ir:*dynamic-environment* (cleavir-ir:dynamic-environment instruction)))
             (cleavir-ir:insert-instruction-after
              (make-instance 'cleavir-ir:fixed-to-multiple-instruction
                             :inputs
                             (mapcar
                              (lambda (x)
                                (cleavir-ir:make-load-time-value-input
                                 (list 'quote x)))
                              values)
                             :outputs (list new-output))
              instruction)
             (dolist (use (cleavir-ir:using-instructions output))
               (nsubstitute new-output
                            output
                            (cleavir-ir:inputs use))))
           :overdefined))))

(defmethod constant-fold ((instruction cleavir-ir:funcall-instruction) args)
  (function-fold instruction (first args) (rest args)))

(defmethod constant-fold ((instruction cleavir-clisp-ir:system-funcall-instruction) args)
  (function-fold instruction (function-name instruction) args))

(defmethod constant-fold ((instruction cleavir-ir:assignment-instruction) args)
  (first args))

(defmethod constant-fold ((instruction cleavir-ir:multiple-to-fixed-instruction) args)
  (first args))

(defmethod constant-fold ((instruction cleavir-ir:fdefinition-instruction) args)
  (fdefinition (first args)))

;;; Hack until I figure out what the right way to do this is
(defun constant-input-p (input)
  (and (typep input 'cleavir-ir:load-time-value-input)
       (eq (first (cleavir-ir:form input)) 'quote)))

(defun constant-input-value (input)
  (second (cleavir-ir:form input)))

;;; Perform conditional constant propagation. Algorithm adapted from
;;; Appel. Can only be done on an edge split graph.
(defun ssa-constant-propagation-analyze (enter-instruction)
  (let* ((basic-blocks (cleavir-basic-blocks:basic-blocks enter-instruction))
         (instruction-basic-blocks (cleavir-basic-blocks:instruction-basic-blocks basic-blocks))
         (variable-values (make-hash-table))
         (executablep (make-hash-table))
         (variable-worklist (data-of-type enter-instruction 'cleavir-ir:datum))
         (starting-basic-blocks (starting-basic-blocks basic-blocks))
         (block-worklist (reverse basic-blocks)))
    ;; Initialize variable and block information
    (dolist (v variable-worklist)
      (setf (gethash v variable-values)
            (cond ((constant-input-p v)
                   (constant-input-value v))
                  ((typep (first (cleavir-ir:defining-instructions v))
                          'cleavir-ir:enter-instruction)
                   :overdefined)
                  (t :undefined))))
    (dolist (block block-worklist)
      (setf (gethash block executablep) nil))
    ;; Mark all starting blocks executable
    (dolist (starting-block starting-basic-blocks)
      (setf (gethash starting-block executablep) t))
    (flet ((variable-value (v)
             (gethash v variable-values))
           ((setf variable-value) (new-val v)
             (unless (eql new-val (gethash v variable-values))
               (setf (gethash v variable-values) new-val)
               (pushnew v variable-worklist)))
           (mark-executable (b)
             (unless (gethash b executablep)
               (setf (gethash b executablep) t)
               (pushnew b block-worklist)
               (dolist (succ (cleavir-basic-blocks:successors b))
                 (when (gethash succ executablep)
                   (pushnew succ block-worklist)))))
           (instruction-executable (instruction)
             (gethash (gethash instruction instruction-basic-blocks)
                      executablep)))
      (flet ((propagate-constants (instruction)
               (when (instruction-executable instruction)
                 (when (typep instruction 'cleavir-ir:eq-instruction)
                   (let ((successors (mapcar (lambda (s)
                                               (gethash s instruction-basic-blocks))
                                             (cleavir-ir:successors instruction))))
                     (cond ((member :overdefined
                                    (cleavir-ir:inputs instruction)
                                    :key #'variable-value)
                            (mapc #'mark-executable successors))
                           ((not (member :undefined
                                         (cleavir-ir:inputs instruction)
                                         :key #'variable-value))
                            (if (reduce #'eql
                                        (cleavir-ir:inputs instruction)
                                        :key #'variable-value)
                                (mark-executable (first successors))
                                (mark-executable (second successors)))))))
                 (when (cleavir-ir:outputs instruction)
                   ;; for M->F
                   (when (rest (cleavir-ir:outputs instruction))
                     (dolist (output (rest (cleavir-ir:outputs instruction)))
                       (setf (variable-value output) :overdefined)))
                   (let ((v (first (cleavir-ir:outputs instruction))))
                     ;; VALUES location hack. SSA doesn't apply for
                     ;; values locations, so we kludge it and decline
                     ;; to do anything with them if they have more
                     ;; than one defining instruction
                     (when (and (typep v 'cleavir-ir:values-location)
                                (rest (cleavir-ir:defining-instructions v)))
                       (setf (variable-value v) :overdefined)
                       (return-from propagate-constants))
                     (cond
                       ((constant-foldable instruction
                                           (mapcar #'variable-value (cleavir-ir:inputs instruction)))
                        ;; v <- x op y where V[x] = c2, V[y] = c2
                        (when (every (lambda (x)
                                       (not (member (variable-value x)
                                                    '(:undefined :overdefined))))
                                     (cleavir-ir:inputs instruction))
                          (setf (variable-value v)
                                (constant-fold instruction
                                               (mapcar #'variable-value (cleavir-ir:inputs instruction)))))
                        (when (member :overdefined (cleavir-ir:inputs instruction)
                                      :key #'variable-value)
                          (setf (variable-value v) :overdefined)))
                       ((typep instruction 'cleavir-ir:phi-instruction)
                        (let* ((phi-block (gethash instruction instruction-basic-blocks))
                               (executable-inputs
                                 (let (inputs)
                                   (dolist (predecessor (cleavir-basic-blocks:predecessors phi-block))
                                     (when (gethash predecessor executablep)
                                       (push (nth (position (cleavir-basic-blocks:last-instruction predecessor)
                                                            (phi-predecessors instruction))
                                                  (cleavir-ir:inputs instruction))
                                             inputs)))
                                   inputs)))
                          ;; Equation 5

                          (when executable-inputs
                            (unless (reduce #'eql executable-inputs :key #'variable-value)
                              (setf (variable-value v) :overdefined))
                            ;; Equation 8

                            (when (member :overdefined executable-inputs :key #'variable-value)
                              (setf (variable-value v) :overdefined))
                            ;; Equation 9
                            (let ((inputs-with-value (remove-if (lambda (j)
                                                                  (eq (variable-value j)
                                                                      :undefined))
                                                                executable-inputs)))
                              (when (and inputs-with-value
                                         (reduce #'eql inputs-with-value :key #'variable-value))
                                (setf (variable-value v)
                                      (variable-value (first inputs-with-value))))))))
                       ;; Equation 7
                       (t (setf (variable-value v) :overdefined))))))))
        (loop
          (when (and (null variable-worklist)
                     (null block-worklist))
            (return))
          ;; Equation 3
          (when block-worklist
            (let* ((block (pop block-worklist))
                   (successors (cleavir-basic-blocks:successors block)))
              (when (gethash block executablep)
                (when (and successors (null (rest successors)))
                  (mark-executable (first successors)))
                (when (and (rest successors))
                  (let ((last (cleavir-basic-blocks:last-instruction block)))
                    (typecase last
                      (cleavir-ir:catch-instruction
                       (mapc #'mark-executable successors))
                      (cleavir-ir:eq-instruction
                       (when (member :overdefined (cleavir-ir:inputs last)
                                     :key #'variable-value)
                         (mapc #'mark-executable successors))
                       (when (not (member :undefined (cleavir-ir:inputs last)
                                          :key #'variable-value))
                         (if (reduce #'eql
                                     (cleavir-ir:inputs last)
                                     :key #'variable-value)
                             (mark-executable (first successors))
                             (mark-executable (second successors))))))))
                (cleavir-basic-blocks:map-basic-block-instructions #'propagate-constants block))))
          (when variable-worklist
            (let ((x (pop variable-worklist)))
              (dolist (instruction (cleavir-ir:using-instructions x))
                (propagate-constants instruction)))))))
    (values executablep variable-values)))

(defun delete-block (basic-block)
  ;; Cleave off a basic block by nullifying the
  ;; predecessors/successors the first and last instructions of the
  ;; basic block. If the preceding block branches, delete the branch
  ;; instruction.
  (with-accessors ((first cleavir-basic-blocks:first-instruction)
                   (last cleavir-basic-blocks:last-instruction))
      basic-block
    (dolist (predecessor (cleavir-ir:predecessors first))
      (cond ((rest (cleavir-ir:successors predecessor))
             (let ((branch-predecessor (first (cleavir-ir:predecessors predecessor)))
                   (proven-successor (first (remove first (cleavir-ir:successors predecessor)))))
               ;; We can delete this branch instruction, redirecting to the
               ;; proven successor.
               (nsubstitute branch-predecessor
                            predecessor
                            (cleavir-ir:predecessors proven-successor))
               (nsubstitute proven-successor
                            predecessor
                            (cleavir-ir:successors branch-predecessor))))
            (t
             ;; I am pretty sure this can only happen when the
             ;; predecessor is not executable either, so it doesn't
             ;; matter what happens here.
             (setf (cleavir-ir:successors predecessor) '()))))
    (setf (cleavir-ir:predecessors first) '())
    (dolist (successor (cleavir-ir:successors last))
      (when (typep successor 'cleavir-ir:phi-instruction)
        (let ((j (position last (cleavir-ir:predecessors successor))))
          (map-phi-cluster
           (lambda (phi)
             (setf (cleavir-ir:inputs phi)
                   (delete (nth j (cleavir-ir:inputs phi))
                           (cleavir-ir:inputs phi)
                           :start j :end (1+ j))))
           successor)))
      (setf (cleavir-ir:predecessors successor)
            (delete last (cleavir-ir:predecessors successor))))
    (setf (cleavir-ir:successors last) '())))

(defun ssa-constant-propagation (enter-instruction)
  (multiple-value-bind (executablep variable-values)
      (ssa-constant-propagation-analyze enter-instruction)
    (maphash (lambda (basic-block executablep)
               (unless executablep
                 (delete-block basic-block)))
             executablep)
    (dolist (phi (cleavir-ir:instructions-of-type enter-instruction 'cleavir-ir:phi-instruction))
      (when (= (length (phi-predecessors phi)) 1)
        (let ((input (first (cleavir-ir:inputs phi)))
              (output (first (cleavir-ir:outputs phi))))
          (dolist (output-use (cleavir-ir:using-instructions output))
            (nsubstitute input
                         output
                         (cleavir-ir:inputs output-use)))
          (change-class phi 'cleavir-ir:nop-instruction
                        :inputs '()
                        :outputs '()))))
    (maphash (lambda (variable value)
               (unless (member value '(:undefined :overdefined))
                 (let ((defining-instruction (first (cleavir-ir:defining-instructions variable))))
                   ;; really, we should be checking if the containing
                   ;; block is executable
                   (when (and defining-instruction
                              (cleavir-ir:successors defining-instruction))
                     (change-class defining-instruction 'cleavir-ir:nop-instruction
                                   :inputs '()
                                   :outputs '())))
                 (change-class variable 'cleavir-ir:load-time-value-input
                               :form (list 'quote value)
                               :read-only-p t)))
             variable-values))
  (cleavir-ir:reinitialize-data enter-instruction))
