(in-package #:cleavir-ast-to-hir)

;;;; Conversion methods for CLISP specific ASTs to HIR.

(defmethod compile-ast ((ast cleavir-clisp-ast:system-funcall-ast) context)
  (with-accessors ((results results)
		   (successors successors))
      context
    (assert-context ast context nil 1)
    (let* ((all-args (cleavir-ast:argument-asts ast))
	   (temps (make-temps all-args)))
      (compile-arguments
       all-args
       temps
       (if (typep results 'cleavir-ir:values-location)
	   (make-instance 'cleavir-clisp-ir:system-funcall-instruction
                          :function-name (cleavir-clisp-ast:system-funcall-ast-function-name ast)
                          :inputs temps
                          :outputs (list results)
                          :successors successors)
	   (let* ((values-temp (make-instance 'cleavir-ir:values-location)))
	     (make-instance 'cleavir-clisp-ir:system-funcall-instruction
                            :function-name (cleavir-clisp-ast:system-funcall-ast-function-name ast)
                            :inputs temps
                            :outputs (list values-temp)
                            :successors
                            (list (cleavir-ir:make-multiple-to-fixed-instruction
                                   values-temp results (first successors))))))
       context))))
