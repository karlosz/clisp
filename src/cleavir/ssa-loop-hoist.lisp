(in-package #:cleavir-clisp-ir)

(defgeneric hoistable (instruction inputs))

(defmethod hoistable (instruction inputs) nil)

(defmethod hoistable ((instruction system-funcall-instruction) inputs)
  (and (cleavir-clisp::function-foldable-p (function-name instruction))
       ;; We can't hoist division
       (not (eq (function-name instruction) 'cl:/))))

(defmethod hoistable ((instruction cleavir-ir:multiple-to-fixed-instruction) inputs)
  t)

(defun loop-instructions (loop)
  (let (instructions)
    (dolist (basic-block (nodes loop))
      (cleavir-basic-blocks:map-basic-block-instructions
       (lambda (instruction)
         (push instruction instructions))
       basic-block))
    instructions))

(defun loop-invariant-definitions (loop instruction-basic-blocks)
  (let ((worklist (loop-instructions loop))
        (loop-invariant-p (make-hash-table :test #'eq))
        loop-invariant-definitions)
    (labels ((instruction-in-loop (instruction)
               (member (gethash instruction instruction-basic-blocks)
                       (nodes loop)))
             (loop-invariant-p (datum)
               (or (typep datum 'cleavir-ir:load-time-value-input)
                   (not (member (gethash (first (cleavir-ir:defining-instructions datum))
                                         instruction-basic-blocks)
                                (nodes loop)))
                   (gethash datum loop-invariant-p)))
             (mark-loop-invariant (datum)
               (unless (gethash datum loop-invariant-p)
                 (setf (gethash datum loop-invariant-p) t)
                 (dolist (use (cleavir-ir:using-instructions datum))
                   (when (instruction-in-loop use)
                     (pushnew use worklist))))))
      (loop (when (null worklist)
              (return (nreverse loop-invariant-definitions)))
            (let ((instruction (pop worklist)))
              (when (hoistable instruction (cleavir-ir:inputs instruction))
                (when (every #'loop-invariant-p (cleavir-ir:inputs instruction))
                  (mark-loop-invariant (first (cleavir-ir:outputs instruction)))
                  (push instruction loop-invariant-definitions))))))))

;; Relies on edge splitting to create landing pads for hoisted
;; instructions
(defun ssa-loop-hoist (initial-instruction)
  (let* ((basic-blocks (cleavir-basic-blocks:basic-blocks initial-instruction))
         (instruction-basic-blocks (cleavir-basic-blocks:instruction-basic-blocks basic-blocks)))
    (dolist (starting-basic-block (starting-basic-blocks basic-blocks))
      (let ((loops (compute-loops
                    starting-basic-block
                    #'cleavir-basic-blocks:successors)))
        (dolist (loop loops)
          (dolist (instruction (loop-invariant-definitions loop instruction-basic-blocks))
            (let ((inputs (cleavir-ir:inputs instruction))
                  (outputs (cleavir-ir:outputs instruction))
                  (target-block (first (cleavir-basic-blocks:predecessors (header-node loop)))))
              (cleavir-ir:delete-instruction instruction)
              (cleavir-ir:insert-instruction-before
               (reinitialize-instance
                instruction
                :successors '()
                :predecessors '()
                :inputs inputs
                :outputs outputs)
               (cleavir-basic-blocks:last-instruction target-block))
              ;; Update information for consistency
              (setf (cleavir-ir:defining-instructions (first outputs))
                    (list instruction))
              (setf (gethash instruction instruction-basic-blocks)
                    target-block)
              (when (eq (cleavir-basic-blocks:last-instruction target-block)
                        (cleavir-basic-blocks:first-instruction target-block))
                (setf (cleavir-basic-blocks:first-instruction target-block)
                      instruction)))))))))
