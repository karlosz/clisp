(in-package #:cleavir-clisp-ir)

(defvar *number-memo*)
(defvar *number*)

(defun singleton-p (class)
  (and class (null (rest class))))

(defun %input-number (thing pool)
  (first (find-if (lambda (class)
                    (find thing (cdr class) :test #'equal))
                  pool)))

(defun add-%input-number (number thing pool)
  (let ((pool (copy-list pool))
        (assoc (assoc number pool)))
    (if assoc
        (progn
          (push thing (rest assoc)))
        (push (list number thing) pool))
    pool))

(defun delete-singletons (pool)
  ;; FIXME delete the old value bindings involving this class since
  ;; they are no longer available.
  (delete-if-not #'cdr pool))

(defun shadow-binding (output pool)
  (dolist (class pool)
    (setf (rest class) (delete output (rest class)))))

(defgeneric make-value-expression (instruction pool))

(defmethod make-value-expression ((instruction cleavir-clisp-ir:system-funcall-instruction) pool)
  (let ((inputs (cleavir-ir:inputs instruction)))
    (dolist (input inputs)
      (or (%input-number input pool)
          (setf pool
                (add-%input-number (incf *number*) input pool))))
    (values (cons (cleavir-clisp-ir:function-name instruction) (mapcar (lambda (input) (%input-number input pool)) inputs))
            pool)))

(defun class-meet (class1 class2 pool1 pool2)
  (let ((memo (gethash (list (first class1)
                             (first class2)) *number-memo*)))
    (when memo
      (return-from class-meet memo)))
  (let (meet-class)
    (dolist (e (intersection (rest class1) (rest class2) :test #'equal))
      (push e meet-class)
      (when (= (first class1) (first class2))
        (push (first class1) meet-class)))
    (dolist (e1 (rest class1))
      (when (listp e1)
        (dolist (e2 (rest class2))
          (when (listp e2)
            (when (and (not (equal e1 e2))
                       (eql (first e1) (first e2))
                       (= (length e1) (length e2)))
              (let ((ck (mapcar (lambda (num1 num2)
                                  (class-meet (assoc num1 pool1)
                                              (assoc num2 pool2)
                                              pool1 pool2))
                                (rest e1) (rest e2))))
                (when (not (some #'null ck))
                  (print 'hey)
                  (push (cons (first e1) (mapcar #'first ck))
                        meet-class))))))))
    (setf (gethash (list (first class1)
                         (first class2))
                   *number-memo*)
          (and meet-class
               (if (numberp (first meet-class))
                   meet-class
                   (cons (incf *number*) meet-class))))))

(defun pool-meet (pool1 pool2)
  (let (pool-meet)
    (dolist (class1 pool1)
      (dolist (class2 pool2)
        (let ((class-meet (class-meet class1 class2 pool1 pool2)))
          (when class-meet
            (push class-meet pool-meet)))))
    (setf pool-meet (delete-singletons pool-meet))))

(defun value-number (enter-instruction)
  (let ((in (make-hash-table :test #'eq))
        (out (make-hash-table :test #'eq))
        (fixpointp nil))
    (cleavir-ir:map-instructions
     (lambda (instruction)
       (setf (gethash instruction out) nil
             (gethash instruction in) nil))
     enter-instruction)
    (loop (when fixpointp
            (return))
          (let ((*number* 0)
                (*number-memo* (make-hash-table :test #'equal)))
            (cleavir-ir:map-instructions
             (lambda (instruction)
               (let ((old-out (gethash instruction out)))
                 (setf (gethash instruction in)
                       (if (rest (cleavir-ir:predecessors instruction))
                           (reduce #'pool-meet
                                   (cleavir-ir:predecessors instruction)
                                   :key (lambda (i)
                                          (gethash i out)))
                           (gethash (first (cleavir-ir:predecessors instruction))
                                    out)))
                 (setf (gethash instruction out)
                       (transfer* instruction (gethash instruction in)))
                 (setf fixpointp
                       (tree-equal old-out (gethash instruction out)))))
             enter-instruction)))
    out))

(defgeneric transfer* (instruction in-pool))

(defmethod transfer* (instruction in-pool))

(defmethod transfer* (instruction in-pool)
  (let ((outputs (cleavir-ir:outputs instruction))
        (pool in-pool))
    (dolist (output outputs)
      (shadow-binding output pool))
    (setf pool (delete-singletons pool))
    (dolist (output outputs)
      (setf pool (add-%input-number (incf *number*) output pool)))
    pool))

(defmethod transfer* ((instruction cleavir-ir:assignment-instruction) in-pool)
  (let* ((input (first (cleavir-ir:inputs instruction)))
         (output (first (cleavir-ir:outputs instruction)))
         (succ (first (cleavir-ir:successors instruction)))
         (pool in-pool))
    (shadow-binding output pool)
    (if  (%input-number input pool)
         (add-%input-number (%input-number input pool)
                            output
                            pool)
         (add-%input-number (incf *number*)
                            output
                            pool))))

(defmethod transfer* ((instruction cleavir-clisp-ir:system-funcall-instruction) in-pool)
  (if (cleavir-clisp::function-foldable-p (function-name instruction))  
      (let ((output (first (cleavir-ir:outputs instruction)))
            (pool in-pool))
        (shadow-binding output pool)
        (setf pool (delete-singletons pool))
        (let* ((expr (multiple-value-bind (expr pool*)
                         (make-value-expression instruction pool)
                       (setf pool pool*)
                       expr))
               (input-number (%input-number expr pool)))
          (if input-number
              (progn
                (format t "~&Redundant instruction ~a" instruction)
                (setf pool (add-%input-number input-number output pool)))
              (progn (setf pool (add-%input-number (incf *number*)
                                                   expr
                                                   pool))
                     (setf pool (add-%input-number *number*
                                                   output
                                                   pool)))))
        pool)
      (call-next-method)))
