(cl:in-package #:cleavir-clisp)

;;;; Translation to CLISP LAP code.

;;;; We translate the transformed HIR graph into LAP code, in units of
;;;; basic blocks. The HIR graph needs to have the following
;;;; properties:

;;;; 1. The graph needs to have unique lexical locations for every
;;;; possible stack location. This is acheived by putting the graph in
;;;; SSA form and implementing phi functions to give extra stack
;;;; locations at merge points.

;;;; 2. The graph needs to be in edge-split form. This allows the
;;;; basic blocks to get translated in a linear time pass in terms of
;;;; propagating information about stack indices.

;;;; 3. Closure variables have to be converted with CELLs.


;;; The first argument to this function is an instruction that has a
;;; single successor.  Whether a GO is required at the end of this
;;; function is determined by the code layout algorithm.  
;;; 
;;; The inputs are forms to be evaluated.  The outputs are symbols
;;; that are names of variables.
(defgeneric translate-simple-instruction (instruction inputs outputs))

(defgeneric translate-branch-instruction (instruction inputs outputs successors))

(defvar *basic-blocks*)
(defvar *instruction-basic-blocks*)
(defvar *tags*)
(defvar *vars*)
;; vector of constants
(defvar *constants*)
;; number of locations allocated on the stack
(defvar *stack-pos*)
(defvar *code-list*)
(defvar *nargs* 0)
;; map from basic block -> start index * end index
(defvar *basic-block-stack-indices*)
;; CLISP needs this because it tacks on keyword constants in the
;; beginning so we need to offset our constant numbering
(defvar *keywords*)
(defvar *instruction-ownerships*)

(defclass stack-location ()
  ((%index :accessor stack-location-index :initarg :index)
   (%fun-constant :accessor fun-constant)))

(defun make-stack-location (index)
  (make-instance 'stack-location :index index))

(defun allocate-stack-location (value)
  (decf *stack-pos*)
  (setf (gethash value *vars*) (make-stack-location *stack-pos*))
  (inst PUSH))

(defun allocate-multiple-values-to-stack (outputs)
  (dolist (output outputs)
    (decf *stack-pos*)
    (setf (gethash output *vars*) (make-stack-location *stack-pos*)))
  ;; NOT A TYPO.
  (inst SYSTEM::NV-TO-STACK (length outputs)))

(defun allocate-values-location (value)
  (setf (gethash value *vars*) (make-instance 'values-location)))

(defun =-stack-location (value1 value2)
  (= (stack-location-index (gethash value1 *vars*))
     (stack-location-index (gethash value2 *vars*))))

(defclass values-location ()
  ((%mvcall :accessor mvcall :initarg :mvcall :initform nil)
   (%stack-pos :accessor values-location-stack-pos :initarg :stack-pos)
   (%nargs :accessor values-location-nargs :initarg :nargs)))

(defclass block-location ()
  ((%index :accessor block-location-index :initarg :index)
   (%stack-index :accessor block-location-stack-index :initarg :stack-index)))

(defclass constant-location ()
  ((%index :accessor constant-location-index :initarg :index)
   (%value-spec :accessor constant-location-value-spec :initarg :value-spec)
   (%value :accessor constant-location-value :initarg :value)
   (%closurep :accessor constant-location-closurep :initarg :closurep :initform nil)
   (%block-tags :accessor constant-location-block-tags :initarg :block-tags :initform nil)))

(defun make-constant-location (value &key value-spec closurep block-tags)
  (assert (not (position value *constants*)))
  (vector-push-extend value *constants*)
  (make-instance 'constant-location
                 :index (+ (position value *constants*)
                           *keywords*)
                 :value-spec value-spec
                 :value value
                 :closurep closurep
                 :block-tags block-tags))

(defun compute-constant-location (datum)
  (etypecase datum
    (cleavir-ir:load-time-value-input
     (make-constant-location datum
                             :value-spec
                             (if (eq (first (cleavir-ir:form datum))
                                     'quote)
                                 (system::make-const
                                  :value (second (cleavir-ir:form datum))
                                  :horizon :value)
                                 (system::make-const
                                  :value (eval (cleavir-ir:form datum))
                                  :form (cleavir-ir:form datum)
                                  :ltv-form `(load-time-value ,(cleavir-ir:form datum))
                                  :horizon :all))))
    (cleavir-ir:constant-input
     (make-constant-location datum
                             :value-spec
                             (system::make-const
                              :value (cleavir-ir:value datum)
                              :horizon :value)))))

;;; Compute the relative position on the stack at this point.
(defun stack-offset (index) (- index *stack-pos*))

;;; Given a translated datum held in *vars*, load the location into
;;; the VALUES1 register.
(defun load-location (location)
  (etypecase location
    (values-location
     ;; check if the value is really still in values
     (when (mvcall location)
       (let ((new-stack-pos (values-location-stack-pos location)))
         (emit-skip (- (- new-stack-pos (1+ (values-location-nargs location)))
                       *stack-pos*))
         (inst SYSTEM::MVCALL)
         (setf *stack-pos* new-stack-pos))))
    (constant-location
     (inst SYSTEM::CONST
           (constant-location-index location)
           (constant-location-value-spec location)))
    (block-location
     (inst LOAD (stack-offset (block-location-stack-index location))))
    (stack-location
     (inst LOAD (stack-offset (stack-location-index location))))))

(defun load-value (value)
  (load-location (gethash value *vars*)))

;;; Like LOAD-VALUE, but for store.
(shadow 'store-value)
(defun store-value (value)
  (inst SYSTEM::STORE (stack-offset (stack-location-index
                                     (gethash value *vars*)))))

(defun push-value (value)
  (load-value value)
  (decf *stack-pos*)
  (inst push))

(defmacro inst (inst &rest args)
  `(push (list ',inst ,@args) *code-list*))

(defun label (label)
  (push label *code-list*))

(defun emit-SKIP (amount)
  (assert (not (minusp amount)))
  (unless (zerop amount)
    (inst SYSTEM::SKIP amount)
    (incf *stack-pos* amount)))

;;; We translate constant data by allocating them into the constant
;;; vector before translating the using instruction.
(defun translate-datum (datum)
  (typecase datum
    (cleavir-ir:load-time-value-input
     (unless (or (nth-value 1 (gethash datum *vars*))
                 ;; We don't emit a NIL input to an eq instruction
                 ;; into the constant vector.
                 (and (eq (second (cleavir-ir:form datum)) nil)
                      (eq (first (cleavir-ir:form datum)) 'quote)
                      (null (rest (cleavir-ir:using-instructions datum)))
                      (typep (first (cleavir-ir:using-instructions datum)) 'cleavir-ir:eq-instruction)
                      (eq datum (second (cleavir-ir:inputs
                                         (first (cleavir-ir:using-instructions datum)))))))
       (setf (gethash datum *vars*)
             (compute-constant-location datum))))
    (cleavir-ir:constant-input
     (assert (eq (cleavir-ir:value datum) nil))
     (setf (gethash datum *vars*)
           (compute-constant-location datum)))))

(defun allocate-lambda-list-item-location (item)
  (setf (gethash item *vars*)
        (make-instance 'stack-location
                       :index *stack-pos*))
  (decf *stack-pos*))

(defun translate-lambda-list-item (item)
  (declare (special *state*))
  (cond ((symbolp item)
	 (setf *state* item))
	((consp item)
         (allocate-lambda-list-item-location
          (ecase (length item)
            (2 (first item))
            (3
             ;; for some reason CLISP allocates a place in the
             ;; constant vector for the key parameter at an odd time
             (incf *keywords*)
             (second item)))))
	(t (allocate-lambda-list-item-location item))))

(defun translate-lambda-list (lambda-list)
  ;; Allocate stack locations for the formal parameters.
  (setf *nargs* (length (remove-if #'symbolp lambda-list)))
  (setf *stack-pos* (length (remove-if #'symbolp lambda-list)))
  (mapcar #'translate-lambda-list-item lambda-list))

(defun layout-basic-block (basic-block basic-blocks &optional initial-code)
  (with-accessors ((first cleavir-basic-blocks:first-instruction)
                   (last cleavir-basic-blocks:last-instruction)
                   (owner cleavir-basic-blocks:owner))
      basic-block
    (declare (ignore owner))
    ;; hacking the weird semantics of block open
    (when (and (null (rest (cleavir-ir:predecessors first)))
               (typep (first (cleavir-ir:predecessors first))
                      'cleavir-ir:catch-instruction)
               (eq (first (cleavir-ir:successors
                           (first (cleavir-ir:predecessors first))))
                   first))
      (decf *stack-pos* 3))
    (let ((*code-list* initial-code))
      (loop for instruction = first
	      then (first (cleavir-ir:successors instruction))
	    for inputs = (cleavir-ir:inputs instruction)
	    for outputs = (cleavir-ir:outputs instruction)
	    until (eq instruction last)
	    do (mapc #'translate-datum inputs)
               (translate-simple-instruction
                instruction inputs outputs))
      (let* ((inputs (cleavir-ir:inputs last))
	     (outputs (cleavir-ir:outputs last))
	     (successors (cleavir-ir:successors last))
	     (successor-tags (loop for successor in successors
				   collect (gethash successor *tags*))))
        (mapc #'translate-datum inputs)
	(if (= (length successors) 1)
            (progn (translate-simple-instruction
                    last inputs outputs)
                   ;; check if the successors have starting stack
                   ;; indices yet. if they do, assert that they
                   ;; are all the same and SKIP the correct amount
                   (dolist (successor (cleavir-basic-blocks:successors basic-block))
                     (when (nth-value 1 (gethash successor *basic-block-stack-indices*))
                       ;; FIXME: actually do the assertion
                       (destructuring-bind (start . end)
                           (gethash successor *basic-block-stack-indices*)
                         (emit-skip (- (- *stack-pos* start))))))
                   (inst SYSTEM::JMP (gethash (first successors) *tags*)))
            (translate-branch-instruction
             last inputs outputs successor-tags)))
      (annotate-basic-block *code-list* basic-block))))

(defun layout-function-prelude (initial-instruction)
  ;;; FUNCTION PRELUDE
  ;; check if the function that we are compiling is a prototype or
  ;; not.  i.e. whether we need to add a spot in the CONSTS vector
  ;; to hold the closure environment vector/block tags
  (let ((initializer (gethash initial-instruction *vars*))
        (*code-list* nil))
    (when initializer
      (multiple-value-bind (non-block-inputs block-inputs)
          (segregate-closure-inputs (rest (cleavir-ir:inputs initializer)))
        (when non-block-inputs
          (vector-push-extend 'venv-placeholder *constants*))
        (dolist (block-input block-inputs)
          (vector-push-extend (gethash block-input *vars*) *constants*))))
    *code-list*))

(defun layout-procedure (initial-instruction)
  (let* ((basic-blocks (remove initial-instruction
			       *basic-blocks*
			       :test-not #'eq :key #'cleavir-basic-blocks:owner))
	 (first (find initial-instruction basic-blocks
		      :test #'eq :key #'cleavir-basic-blocks:first-instruction))
	 (rest (remove first basic-blocks :test #'eq))
         (*constants* (make-array 10 :fill-pointer 0 :adjustable t))
         (*stack-pos* 0)
         (*nargs* *nargs*)
         (*keywords* 0)
         (*basic-block-stack-indices* (make-hash-table)))
    ;; Assign tags to all basic blocks except the first one
    (loop for block in rest
	  for instruction = (cleavir-basic-blocks:first-instruction block)
	  do (setf (gethash instruction *tags*) (gensym)))
    (translate-lambda-list (cleavir-ir:lambda-list initial-instruction))
    (let ((prelude (layout-function-prelude initial-instruction)))
      ;; Now we have to make sure we translate everything in the right
      ;; order.
      ;; Generally, dominating nodes must be translated first in order
      ;; to propogate the correct stack indices in one pass.

      ;; We also
      ;; check the dominance frontiers to see which siblings must be
      ;; translated first.

      ;; The equations we need to satisfy are:
      ;; start(n) = end(imdom(n)) +/- (number of phi functions)
      ;; because we want that many stack locations
      ;; start(n) = end(pred(n)) ->
      ;; end(n) = start(succ(n)) which will be used to calculate
      ;; the number of SKIP instructions needed.

      ;; because we are using edge split ssa form, we are guaranteed
      ;; there are no contradictions. i think? write a formal proof

      ;; the NOPs added by edge split correspond exactly to the
      ;; compensatory amount of SKIP needed

      ;; That's why we translate the basic blocks in the order that we
      ;; do.

      ;; The first block dominates all others
      (layout-basic-block first basic-blocks prelude)
      (setf (gethash first *basic-block-stack-indices*)
            (cons 'initial *stack-pos*))
      (let ((dominance-frontiers
              (cleavir-dominance:dominance-frontiers
               first
               #'cleavir-basic-blocks:successors))
            (dominator-tree
              (cleavir-dominance:dominance-tree
               first
               #'cleavir-basic-blocks:successors))
            (translated (list first)))
        ;; we try to translate each block. if it has untranslated parent dominators
        ;; or dominance frontier nodes we must translate those first
        ;; dominators first just for chaving the correct stack locations
        ;; and dominator frontiers for aliasing the correct phi nodes
        (labels ((translate-1 (block)
                   (when (member block translated)
                     (return-from translate-1))
                   (let ((immediate-dominator (cleavir-dominance:immediate-dominator dominator-tree block))
                         (frontiers (remove block (cleavir-dominance:dominance-frontier dominance-frontiers block) :test #'eq)))
                     (unless (or (null immediate-dominator)
                                 (member immediate-dominator translated))
                       (translate-1 immediate-dominator))
                     (dolist (frontier frontiers)
                       (unless (member frontier translated)
                         (translate-1 frontier)))
                     (let* ((start
                              ;; i hope this is correct!  if the
                              ;; prdecessors have been translated
                              ;; already, that means that we don't
                              ;; include the phi functions
                              (- (cdr (gethash immediate-dominator *basic-block-stack-indices*))
                                 (if (some (lambda (b)
                                             (member b translated))
                                           (cleavir-basic-blocks:predecessors block))
                                     0
                                     (count-if-not #'cleavir-clisp-ir::catch-closed-phi
                                                   (cleavir-clisp-ir::basic-block-phi-functions
                                                    block)))))
                            (*stack-pos* start))
                       (layout-basic-block block basic-blocks
                                           (list (gethash (cleavir-basic-blocks:first-instruction block) *tags*)))
                       (push block translated)
                       (setf (gethash block *basic-block-stack-indices*)
                             (cons start *stack-pos*))))))
          (mapc #'translate-1 rest))
        ;; let all the block tag and closure vars together be called
        ;; outer vars. CLISP expects the index past this to be the
        ;; keyword offset. it will automatically make dummy places in
        ;; the const vector if you set the fnode properties correctly
        (let ((normal-consts)
              (ltv-forms)
              (block+tagbodys)
              (venvc))
          (map nil
               (lambda (c)
                 (etypecase c
                   (cleavir-ir:load-time-value-input
                    (if (eq (first (cleavir-ir:form c))
                            'quote)
                        (progn (push (second (cleavir-ir:form c)) normal-consts)
                               (push nil ltv-forms))
                        (progn (push (eval (cleavir-ir:form c)) normal-consts)
                               (push (cleavir-ir:form c) ltv-forms))))
                   (cleavir-ir:constant-input
                    (push (cleavir-ir:value c) normal-consts)
                    (push nil ltv-forms))
                   (function
                    (push c normal-consts)
                    (push nil ltv-forms))
                   (system::fnode
                    (push (system::fnode-code c) normal-consts)
                    (push nil ltv-forms))
                   (symbol
                    (assert (or (eq c 'venv-placeholder)
                                (search "block"
                                        (symbol-name c))))
                    (if (eq c 'venv-placeholder)
                        (push 'venv-placeholder venvc)
                        (progn (push c normal-consts)
                               (push nil ltv-forms))))
                   (block-location (push c block+tagbodys))))
               *constants*)
          (values (trace-schedule-blocks (nreverse translated))
                  ;; normal consts
                  (nreverse normal-consts)
                  (nreverse ltv-forms)
                  ;; keyword offset
                  (+ (length venvc) (length block+tagbodys))))))))

(defun translate (initial-instruction)
  (let* ((*basic-blocks* (cleavir-basic-blocks:basic-blocks initial-instruction))
         (*instruction-basic-blocks* (cleavir-basic-blocks:instruction-basic-blocks *basic-blocks*))
         (*instruction-ownerships* (cleavir-hir-transformations:compute-instruction-owners initial-instruction))
	 (*tags* (make-hash-table :test #'eq))
	 (*vars* (make-hash-table :test #'eq)))
    (layout-procedure initial-instruction)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Methods on TRANSLATE-SIMPLE-INSTRUCTION.

(defun normalize-lambda-list (lambda-list)
  ;; try to recover a normal lambda list from an enter instruction
  ;; lambda list
  (mapcar (lambda (x)
            (if (typep x 'cleavir-ir:lexical-location)
                (cleavir-ir:name x)
                (if (consp x)
                    (ecase (length x)
                      (2 (list (cleavir-ir:name (first x))
                               '%dummy-init-form
                               (cleavir-ir:name (second x))))
                      (3 (list (list (first x)
                                     (cleavir-ir:name (second x)))
                               '%dummy-init-form
                               (cleavir-ir:name (third x)))))
                    x)))
          lambda-list))

;; Segregate inputs to closure based on whether they come from catch
;; or not.
(defun segregate-closure-inputs (inputs)
  (values (remove-if #'produced-by-catch inputs)
          (remove-if-not #'produced-by-catch inputs)))

(defun allocate-closure (enclose initializer constant-location)
  (let ((n 0))
    (multiple-value-bind (non-block-values block-values)
        (segregate-closure-inputs (rest (cleavir-ir:inputs initializer)))
      (setf (constant-location-closurep constant-location)
            (not (null non-block-values)))
      (setf (constant-location-block-tags constant-location)
            block-values)
      (when non-block-values
        ;; Allocate the static environment.
        (inst NIL) ; not necessary, useful for debugging!
        (inst SYSTEM::MAKE-VECTOR1&PUSH (length non-block-values))
        (decf *stack-pos*)
        ;; push the closure var vector
        (inst SYSTEM::LOAD&PUSH 0)
        (decf *stack-pos*)
        (incf n))
      (dolist (block-value block-values)
        (let ((block-location (gethash block-value *vars*)))
          (if (slot-boundp block-location '%stack-index)
              (load-location block-location)
              (inst SYSTEM::CONST (block-location-index block-location))))
        (inst SYSTEM::PUSH)
        (decf *stack-pos*)
        (incf n)))
    (inst SYSTEM::COPY-CLOSURE
          (constant-location-index constant-location)
          n)
    (incf *stack-pos* n)
    (let ((closure (first (cleavir-ir:outputs enclose))))
      (allocate-stack-location closure)
      (setf (fun-constant (gethash closure *vars*))
            constant-location))))

;;; We package up a closure as a pair of an environment vector and
;;; code prototype.
(defmethod translate-simple-instruction
    ((instruction cleavir-ir:enclose-instruction) inputs outputs)
  (declare (ignore inputs))
  (let* ((enter-instruction (cleavir-ir:code instruction))
         (fnode (system::make-fnode :enclosing nil
                                    :venvc nil
                                    :name nil))
         (initializer (find-if (lambda (x)
                                 (typep x 'cleavir-ir:initialize-closure-instruction))
                               (cleavir-ir:using-instructions (first outputs))))
         (constant-loc (make-constant-location fnode :value-spec nil)))
    (if (zerop (cleavir-ir:closure-size enter-instruction))
        (setf (gethash (first outputs) *vars*)
              constant-loc)
        (allocate-closure instruction initializer constant-loc))
    ;; we need to really hack up the instruction graph in our
    ;; hash tables since the cleavir flow graph allocates closure
    ;; environments in the enclosed functions while clisp
    ;; allocates closures in the enclosing function
    (setf (gethash enter-instruction *vars*) initializer)
    ;; make the fnode into a function object now
    (multiple-value-bind (code-list consts ltv-forms keyword-offset)
        (layout-procedure enter-instruction)
      (preprocess-fnode fnode (normalize-lambda-list (cleavir-ir:lambda-list enter-instruction)) keyword-offset)
      (setf (sys::fnode-consts fnode) consts)
      (setf (sys::fnode-consts-forms fnode) ltv-forms)
      (let ((SPdepth (sys::SP-depth code-list)))
        ;; determine stack requirements
        (setq code-list (sys::insert-combined-LAPs code-list))
        (sys::create-fun-obj fnode (sys::assemble-LAP code-list) SPdepth)))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:initialize-closure-instruction) inputs outputs)
  (declare (ignore outputs))
  (loop for value in (segregate-closure-inputs (rest inputs))
        for index from 0
        do (load-value value)
           ;; FIXME: Brittle. Assumes that the stack pointer to the
           ;; environment vector is only covered by the actual closure
           ;; on the top of the stack. Can easily be fixed by tacking
           ;; the stack location of make-vector1.
           (inst SYSTEM::STOREC 1 index)))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:enter-instruction) inputs outputs)
  (declare (ignore inputs))
  ;;; Optional and key parameters handling.
  (let ((outputs (copy-list (cleavir-ir:parameters instruction))))
    (dolist (param (remove-if #'symbolp (cleavir-ir:lambda-list instruction)))
      (flet ((bind (thing)
               (setf (gethash (pop outputs) *vars*)
                     (gethash thing *vars*))))
        (if (consp param)
            (multiple-value-bind (arg supplied-p)
                (ecase (length param)
                  (2 (values (first param) (second param)))
                  (3
                   (values (second param) (third param))))
              (inst SYSTEM::BOUNDP (stack-offset
                                    (stack-location-index
                                     (gethash arg *vars*))))
              (allocate-stack-location supplied-p)
              (bind arg)
              (bind supplied-p))
            (bind param))))))

(defmethod translate-simple-instruction
    ((instruction cleavir-clisp-ir::push-instruction) inputs outputs)
  (load-value (first inputs))
  (allocate-stack-location (first outputs)))

;;; This code is outdated. We should almost never have assignment
;;; nodes anymore due to copy propagation and SSA form. We keep the
;;; code in case assignment instructions are ever encountered.
(defun translate-assignoid (inputs outputs)
  ;; mostly because of M->F do we have to worry about this
  (when (null outputs)
    (return-from translate-assignoid))
  ;; when can we alias the input to the output?  i.e. when can we have
  ;; them refer to the same location to avoid unnecessary load and
  ;; stores?

  ;; for both lexical <- lexical and lexical <- constant,
  ;; we have the condition that the target lexical-location
  ;; must have only one def, that is, be owned entirely by
  ;; this instruction
  
  ;; for lexical <- lexical, we have the additional condition that the
  ;; source location must have only 1 def OR the target location has
  ;; only 1 use. otherwise, they have to act indepently with respect
  ;; to what they store.

  ;; NOTE: this ocde was written before I added a HIR pass to
  ;; eliminate useless assignments nodes. However, this is still
  ;; useful for aliasing through M->f and F->M instructions at the moment
  (let ((input (first inputs))
        (output (first outputs)))
    ;; can we alias?
    (cond ((and
            (= (length (cleavir-ir:defining-instructions output)) 1)
            (etypecase input
              ((or cleavir-ir:load-time-value-input
                   cleavir-ir:constant-input)
               t)
              ((or cleavir-ir:lexical-location cleavir-ir:values-location)
               (or (and (= (length (cleavir-ir:defining-instructions input)) 1)
                        (not (typep (gethash input *vars*)
                                    'values-location)))
                   (= (length (cleavir-ir:using-instructions output)) 1)))))
           (setf (gethash output *vars*) (gethash input *vars*)))
          (t
           ;; if we don't alias, check whether we are assigning to an
           ;; existing location or if we must create a new one
           (cond ((not (nth-value 1 (gethash output *vars*)))
                  ;; LOADing and PUSHing the input on the stack
                  ;;  allocates storage for the output and stores the
                  ;;  the initial value of the input that newly
                  ;;  allocated storage
                  (load-value input)
                  (allocate-stack-location output))
                 (t
                  ;; assigning to an already existing stack location
                  ;; is straightfoward
                  (load-value input)
                  (store-value output)))))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:assignment-instruction) inputs outputs)
  (translate-assignoid inputs outputs))

(defun setup-funcall-args (inputs)
  ;; look at the stack and see which need to be pushed on the
  ;; stack. some should already be on the stack in the right place.
  ;; since funcall deallocates the arguments from the stack, make sure
  ;; that the ones that are already there have only one use
  ;; or weren't allocated by the enter instruction - i.e, has positive
  ;; stack index

  ;; this algorithm is essentially detecting whether we can complete a
  ;; substring into a string or not.

  ;; still not as optimal as doing a recursive descent on the original
  ;; source code. consider (+ (* x y) x (+ x y))
  ;; we need some way of pushing atomic forms on the stack at the
  ;; right time
  (let* ((n (length inputs))
         (remaining (copy-list inputs)))
    (flet ((volatile-p (input)
             (and (typep (gethash input *vars*) 'stack-location)
                  (null (rest (cleavir-ir:using-instructions input)))
                  (not (plusp (stack-location-index (gethash input *vars*))))))
           (location (input)
             (stack-offset (stack-location-index
                            (gethash input *vars*)))))
      (when (and (volatile-p (first inputs))
                 (>= (1- n) (location (first inputs))))
        (pop remaining)
        (loop for input in (rest inputs)
              for offset downfrom (1- (location (first inputs))) to 0
              do (unless (and (volatile-p input)
                              (= offset (location input)))
                   (setf remaining inputs)
                   (return))
                 (pop remaining))))
    (dolist (input remaining)
      (push-value input))))

(defun deliver-funcall-values (output)
  (allocate-values-location output))

;; The output of this instruction (a values location, in the HIR
;; sense) is directly returned, but the immediate successor of this
;; instruction is not a RETURN-INSTRUCTION. This means we have to use
;; the CLISP bytecodes MVCALL and MVCALLP and set up the stack
;; differently
(defun values-preserving-funcall (instruction inputs outputs)
  (etypecase instruction
    (cleavir-ir:funcall-instruction (load-value (first inputs)))
    (cleavir-clisp-ir:system-funcall-instruction (inst SYSTEM::CONST
                                      (constant-location-index
                                       (make-constant-location
                                        (fdefinition
                                         (cleavir-clisp-ir:function-name instruction)))))))
  (inst SYSTEM::MVCALLP)
  (let ((args (etypecase instruction
                (cleavir-ir:funcall-instruction (rest inputs))
                (cleavir-clisp-ir:system-funcall-instruction inputs))))
    (setf (gethash (first outputs) *vars*)
          (make-instance 'values-location :mvcall t
                                          :stack-pos *stack-pos*
                                          :nargs (length args)))
    (decf *stack-pos*)
    (dolist (input args)
      (push-value input))))

(defun skipping-nop-and-catch-close (start)
  (do ((instruction start (first (cleavir-ir:successors instruction))))
      ((not (or (typep instruction 'cleavir-ir:nop-instruction)
                (typep instruction 'cleavir-clisp-ir::catch-close-instruction)))
       instruction)))

;; check if a funcall needs its VALUES preserved
(defun values-preserving-p (instruction)
  (when (typep instruction 'cleavir-clisp-ir::system-funcall-instruction)
    (cleavir-clisp-ir::function-name instruction))
  (let* ((output (first (cleavir-ir:outputs instruction)))
         (output-uses (cleavir-ir:using-instructions output))
         (output-defs (cleavir-ir:defining-instructions output)))
    (and (null (rest output-uses))
         ;; I don't think multiple-value-prog1 like effects are possible
         ;; across owners,
         (every (lambda (def)
                  (eq (gethash def *instruction-ownerships*)
                      (gethash (first output-uses) *instruction-ownerships*)))
                output-defs)
         (typep (skipping-nop-and-catch-close (first output-uses)) 'cleavir-ir:return-instruction)
         (not (eq (first output-uses)
                  (skipping-nop-and-catch-close (first (cleavir-ir:successors instruction))))))))

(defun parse-arglist (function)
  (cleavir-code-utilities:parse-ordinary-lambda-list
   (mapcar (lambda (arg)
             (if (keywordp arg)
                 (intern (symbol-name arg))
                 arg))
           (ext:arglist function))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:funcall-instruction) inputs outputs)
  (when (values-preserving-p instruction)
    (values-preserving-funcall instruction inputs outputs)
    (return-from translate-simple-instruction))
  (let ((n (1- (length inputs)))
        (fun-location (gethash (first inputs) *vars*)))
    (when (and (typep fun-location 'stack-location)
               (slot-boundp fun-location '%fun-constant))
      (setf fun-location (fun-constant fun-location)))
    (typecase fun-location
      (constant-location
       (let ((constant-index (constant-location-index fun-location)))
         (if (or (constant-location-closurep fun-location)
                 (constant-location-block-tags fun-location))
             (let* ((fnode (constant-location-value fun-location))
                    (keys (sys::fnode-keywords fnode))
                    (rest-p (sys::fnode-rest-flag fnode))
                    (nargs (length (rest inputs)))
                    (required-count (sys::fnode-req-num fnode))
                    (optional-count (sys::fnode-opt-num fnode)))
               (known-function-arg-setup fnode
                                         (rest inputs)
                                         required-count
                                         rest-p
                                         optional-count
                                         keys
                                         (length keys)
                                         t)
               (load-value (first inputs))
               (if keys
                   (inst SYSTEM::CALLCKEY)
                   (inst SYSTEM::CALLC))
               (incf *stack-pos* (if rest-p
                                     ;; if there is a restp we have listified
                                     ;; and pushed it on the stack
                                     (+ required-count
                                        optional-count
                                        1)
                                     (+ required-count
                                        optional-count
                                        (length keys)))))
             (progn
               (setup-funcall-args (rest inputs))
               (case n
                 (0 (inst SYSTEM::CALL0 constant-index))
                 (1 (inst SYSTEM::CALL1 constant-index))
                 (2 (inst SYSTEM::CALL2 constant-index))
                 (t (inst SYSTEM::CALL n constant-index)))
               (incf *stack-pos* n)))))
      (t
       (setup-funcall-args inputs)
       (inst FUNCALL n)
       (incf *stack-pos* (1+ n)))))
  (deliver-funcall-values (first outputs)))

(defun emit-push-unbound (n)
  (unless (zerop n)
    (inst SYSTEM::PUSH-UNBOUND n)
    (decf *stack-pos* n)))

(defun emit-list (n)
  (if (zerop n)
      (inst NIL)
      (progn
        (inst SYSTEM::LIST n)
        (incf *stack-pos* n))))

(defun known-function-arg-setup (function inputs required-count rest-p optional-count keys key-count listify)
  (let ((nargs (length inputs)))
    (setup-funcall-args (if rest-p
                            (progn (assert (zerop key-count))
                                   inputs)
                            (loop for input in inputs
                                  for n from 0 below (min (+ required-count
                                                             optional-count)
                                                          nargs)
                                  collect input)))
    (when (and rest-p listify)
      (emit-list (- nargs (+ required-count optional-count)))
      (inst SYSTEM::PUSH)
      (decf *stack-pos*))
    (when (plusp optional-count)
      (emit-push-unbound (- (+ required-count
                               optional-count)
                            nargs)))
    (when (plusp key-count)
      (emit-push-unbound key-count)
      (let ((i 0)
            (n 'initial))
        (dolist (input (nthcdr (+ required-count optional-count) inputs))
          (when (zerop (mod i 2))
            (check-type (gethash input *vars*) constant-location)
            (let ((keyword-pos (position (sys::const-value (constant-location-value-spec
                                                            (gethash input *vars*)))
                                         (reverse keys))))
              (unless keyword-pos
                (error "Unknown key parameter."))
              (setf n keyword-pos)))
          (when (= (mod i 2) 1)
            (load-value input)
            (inst SYSTEM::STORE n))
          (incf i))))
    (when (< nargs required-count)
      (error "Not enough arguments supplied to ~a." function))))

(defmethod translate-simple-instruction
    ((instruction cleavir-clisp-ir:system-funcall-instruction) inputs outputs)
  (when (values-preserving-p instruction)
    (values-preserving-funcall instruction inputs outputs)
    (return-from translate-simple-instruction))
  (flet ((arg-count (x)
           (if (consp x)
               (length x)
               0)))
    (let* ((fun-name (cleavir-clisp-ir:function-name instruction))
           (parsed-arglist (parse-arglist fun-name))
           (function-code (gethash fun-name system::function-codes))
           (nargs (length inputs))
           (required-count (arg-count (cleavir-code-utilities:required parsed-arglist)))
           (rest-p (not (eq :none (cleavir-code-utilities:rest-body parsed-arglist))))
           (optional-count (arg-count (cleavir-code-utilities:optionals parsed-arglist)))
           (keys (cleavir-code-utilities:keys parsed-arglist))
           (key-count (arg-count keys)))
      (known-function-arg-setup fun-name inputs required-count rest-p optional-count
                                (if (zerop key-count)
                                    nil
                                    (mapcar #'caar keys))
                                key-count
                                nil)
      (cond ((case fun-name
               ((list)
                (eval `(inst ,fun-name ,nargs)))))
            (rest-p
             (inst SYSTEM::CALLSR
                   (- nargs required-count)
                   (- function-code system::funtabr-index)))
            (t
             (if (< function-code 256)
                 (inst SYSTEM::CALLS1 function-code)
                 (inst SYSTEM::CALLS2 (- function-code 256)))))
      (incf *stack-pos* (if rest-p
                            nargs
                            (+ required-count
                               optional-count
                               key-count)))
      (deliver-funcall-values (first outputs)))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:multiple-value-call-instruction) inputs outputs)
  `(setf ,(first outputs)
	 (multiple-value-list
	  (funcall ,(first inputs)
		   (append ,@(rest inputs))))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:tailcall-instruction) inputs outputs)
  (declare (ignore outputs))
  `(return (funcall ,(first inputs) ,@(rest inputs))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:funcall-no-return-instruction) inputs outputs)
  `(funcall ,(first inputs) ,@(rest inputs)))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:the-instruction) inputs outputs)
  (declare (ignore outputs))
  (gensym)
  ;; doesn't work ltv inputs and such
  #+(or)`(setq ,(first inputs) (the ,(cleavir-ir:value-type instruction)
                              ,(first inputs))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:the-values-instruction) inputs outputs)
  ;; FIXME: annoying to do.
  (gensym))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:dynamic-allocation-instruction) inputs outputs)
  (declare (ignore inputs outputs))
  (gensym))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:fdefinition-instruction) inputs outputs)
  ;; FIXME: don't hardcode this,
  ;; check system::function-codes instead
  (setup-funcall-args inputs)
  (inst SYSTEM::CALLS1 86)
  (incf *stack-pos* 1)
  (if (values-allocatable instruction (first outputs))
      (allocate-values-location (first outputs))
      (allocate-stack-location (first outputs))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:car-instruction) inputs outputs)
  `(setq ,(first outputs)
	 (car ,(first inputs))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:cdr-instruction) inputs outputs)
  `(setq ,(first outputs)
	 (cdr ,(first inputs))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:rplaca-instruction) inputs outputs)
  (declare (ignore outputs))
  `(rplaca ,(first inputs) ,(second inputs)))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:rplacd-instruction) inputs outputs)
  (declare (ignore outputs))
  `(rplacd ,(first inputs) ,(second inputs)))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:fixed-to-multiple-instruction) inputs outputs)
  (when (values-preserving-p instruction)
    (change-class instruction
                  'cleavir-clisp-ir:system-funcall-instruction
                  :function-name 'cl:values)
    (return-from translate-simple-instruction
      (translate-simple-instruction instruction inputs outputs)))
  (let ((input-count (length inputs)))
    (case input-count
      (0
       (inst SYSTEM::VALUES0))
      (1
       (load-value (first inputs))
       ;; It would seem that this belongs in M->F, but it's too wasteful to
       ;; forget secondary values whenever we see M->F. Instead, we should
       ;; forget secondary values here since this instruction really only
       ;; appears before we return.
       ;; RELIES ON SSA HERE
       (when (typep (first (cleavir-ir:defining-instructions (first inputs)))
                    'cleavir-ir:multiple-to-fixed-instruction)
         (inst SYSTEM::VALUES1)))
      (t
       (when (rest inputs)
         (setup-funcall-args inputs)
         (inst SYSTEM::STACK-TO-MV (length inputs))
         (incf *stack-pos* (length inputs))))))
  (allocate-values-location (first outputs)))

(defun values-allocatable (instruction output)
  (and (null (rest (cleavir-ir:using-instructions output)))
       (let ((use (first (cleavir-ir:using-instructions output))))
         (and (not (typep use '(or cleavir-clisp-ir:system-funcall-instruction
                                cleavir-ir:funcall-instruction
                                cleavir-ir:eq-instruction)))
              (eq use
                  (skipping-nop-and-catch-close (first (cleavir-ir:successors instruction))))))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:multiple-to-fixed-instruction) inputs outputs)
  (let ((input (first inputs)))
    (case (length outputs)
      (0 )
      (1
       (let ((output (first outputs)))
         (when (and output (cleavir-ir:using-instructions output))
           (if (gethash input *vars*)
               (load-value input)
               ;; FIXME: Our handling of multiple-values locations is
               ;; extremely weak. It would be more robust to give up
               ;; and save non ephemeral values locations with
               ;; MV-TO-LIST and LIST-TO-MV.
               (warn "Danger! Probably M->F at a join point due to inlining. Exercise extreme caution."))
           (if (values-allocatable instruction output)
               (allocate-values-location output)
               (allocate-stack-location output)))))
      (t (allocate-multiple-values-to-stack outputs)))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:nop-instruction) inputs outputs)
  (declare (ignore inputs outputs))
  (gensym))

(defun produced-by-catch (datum)
  (let ((write (find-if (lambda (i)
                         (typep i 'cleavir-ir:write-cell-instruction))
                        (cleavir-ir:using-instructions datum))))
    (or (and write (typep (first (cleavir-ir:defining-instructions
                                     (second
                                      (cleavir-ir:inputs
                                       write))))
                          'cleavir-ir:catch-instruction))
        (find-if (lambda (i)
                   (typep i 'cleavir-ir:catch-instruction))
                 (cleavir-ir:defining-instructions datum)))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:create-cell-instruction) inputs outputs)
  (declare (ignore inputs))
  ;; we don't create a real cell if we see that a catch instruction
  ;; will produce a value into this cell
  (unless (produced-by-catch (first outputs))
    (inst SYSTEM::NIL&PUSH)
    (inst SYSTEM::LIST 1)
    (allocate-stack-location (first outputs))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:read-cell-instruction) inputs outputs)
  (etypecase (gethash (first inputs) *vars*)
    (stack-location
     (load-value (first inputs))
     (inst SYSTEM::CAR)
     (allocate-stack-location (first outputs)))
    (block-location
     (setf (gethash (first outputs) *vars*)
           (gethash (first inputs) *vars*)))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:write-cell-instruction) inputs outputs)
  (declare (ignore outputs))
  (typecase (gethash (second inputs) *vars*)
    (block-location
     (setf (gethash (first inputs) *vars*)
           (gethash (second inputs) *vars*)))
    (t
     (push-value (first inputs))
     (push-value (second inputs))
     (inst SYSTEM::CALLS1 (gethash 'RPLACA system::function-codes))
     (incf *stack-pos* 2))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:fetch-instruction) inputs outputs)
  (let* ((initializer (gethash (first (cleavir-ir:defining-instructions (first inputs))) *vars*))
         (non-block-inputs (segregate-closure-inputs (rest (cleavir-ir:inputs initializer))))
         (input (nth (cleavir-ir:value (second inputs))
                     (rest (cleavir-ir:inputs initializer))))
         (location (gethash input *vars*)))
    (etypecase location
      ;; NULL arises from mutually recursive labels, functions which
      ;; close over each other.
      ((or null stack-location)
       (inst SYSTEM::LOADV 0 (+ 1 (position input non-block-inputs)))
       (allocate-stack-location (first outputs)))
      (block-location
       (setf (gethash (first outputs) *vars*)
             ;; we alias the cell location defined in the enclosing
             ;; function here into the output cell and increment the
             ;; depth since we have indirected through the closure output
             ;; of the enter instruction.
             (make-instance 'block-location
                            :index (+ (position location *constants*)
                                      *keywords*)))))))

(defun shuffle-overwritten-value (instruction output)
  ;; Since we want to store in a location above the stack, we run the
  ;; risk of overwriting a value that needs to be used by a following
  ;; unphi. Detect this situation, and shuffle that value up in the
  ;; stack.
  (let ((overwritten
          (do ((successor (first (cleavir-ir:successors instruction))
                          (first (cleavir-ir:successors successor))))
              ((typep successor 'cleavir-ir:phi-instruction) nil)
            (when (and (typep (gethash (first (cleavir-ir:inputs successor)) *vars*)
                              'stack-location)
                       (=-stack-location (first (cleavir-ir:inputs successor))
                                         output))
              (return successor)))))
    (when overwritten
      ;; shuffle the input
      (let ((overwritten-input (first (cleavir-ir:inputs overwritten)))
            (new-input (cleavir-ir:make-lexical-location (gensym "SUBBED"))))
        (load-value overwritten-input)
        (allocate-stack-location new-input)
        (do ((successor overwritten (first (cleavir-ir:successors successor))))
            ((typep successor 'cleavir-ir:phi-instruction))
          (setf (cleavir-ir:inputs successor)
                (substitute new-input
                            overwritten-input
                            (cleavir-ir:inputs successor))))))))

(defmethod translate-simple-instruction
    ((instruction cleavir-clisp-ir:unphi-instruction) inputs outputs)
  (let* ((output (first outputs))
         (output-location (gethash output *vars*)))
    (cond ((null output-location)
           (load-value (first inputs))
           (allocate-stack-location output))
          ((and (typep (gethash (first inputs) *vars*) 'stack-location)
                (=-stack-location (first inputs) output)))
          ((= *stack-pos* (1+ (stack-location-index output-location)))
           (load-value (first inputs))
           (allocate-stack-location output))
          (t (shuffle-overwritten-value instruction output)
             (load-value (first inputs))
             (store-value output)))))

(defun phi-read-after-write (instruction)
  ;; we should allocate a new location for the output if it
  ;; is used by one of the unphi functions in the defining
  ;; cluster after the defining instruction
  (some (lambda (input)
          (let ((unphi (first (cleavir-ir:defining-instructions input))))
            (and (typep unphi 'cleavir-clisp-ir:unphi-instruction)
                 (do ((successor (first (cleavir-ir:successors unphi))
                                 (first (cleavir-ir:successors successor))))
                     ((eq successor instruction) nil)
                   (when (eq (first (cleavir-ir:outputs instruction))
                             (first (cleavir-ir:inputs successor)))
                     (return t))))))
        (cleavir-ir:inputs instruction)))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:phi-instruction) inputs outputs)
  ;; This algorithm implements the fact that phi functions alias
  ;; memory locations.
  (let* ((basic-block (gethash instruction *instruction-basic-blocks*))
         (total-phi-functions (cleavir-clisp-ir:count-basic-block-phi-functions basic-block))
         (phi-function-position (cleavir-clisp-ir::phi-function-position instruction basic-block)))
    (let ((defined (remove-if-not (lambda (input)
                                    (nth-value 1 (gethash input *vars*)))
                                  inputs)))
      (if defined
          (let ((location (gethash (first defined) *vars*)))
            (dolist (input inputs)
              (setf (gethash input *vars*) location))
            (cond ((phi-read-after-write instruction)
                   (load-value (first defined))
                   (allocate-stack-location (first outputs)))
                  (t
                   (setf (gethash (first outputs) *vars*)
                         location))))
          (dolist (input inputs)
            (setf (gethash input *vars*)
                  (make-stack-location (+ *stack-pos*
                                          (1- (- total-phi-functions
                                                 phi-function-position)))))
            (setf (gethash (first outputs) *vars*)
                  (gethash input *vars*)))))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:symbol-value-instruction) inputs outputs)
  (inst SYSTEM::GETVALUE
        (constant-location-index (gethash (first inputs) *vars*)))
  (if (values-allocatable instruction (first outputs))
      (allocate-values-location (first outputs))
      (allocate-stack-location (first outputs))))

(defmethod translate-simple-instruction
    ((instruction cleavir-ir:set-symbol-value-instruction) inputs outputs)
  (declare (ignore outputs))
  (load-value (second inputs))
  (inst SYSTEM::SETVALUE (constant-location-index (gethash (first inputs) *vars*))))

(defmethod translate-simple-instruction
    ((instruction cleavir-clisp-ir::catch-close-instruction) inputs outputs)
  (declare (ignore outputs))
  ;; First, unwind the stack to where the block opened.
  (emit-skip (- (- (block-location-stack-index
                    (gethash (first inputs) *vars*))
                   2)
                *stack-pos*))
  (inst SYSTEM::BLOCK-CLOSE)
  (incf *stack-pos* 3))

(defun nil-load-time-value (input)
  (and (typep input 'cleavir-ir:load-time-value-input)
       (eq (first (cleavir-ir:form input)) 'quote)
       (eq (second (cleavir-ir:form input)) nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Methods on TRANSLATE-BRANCH-INSTRUCTION.
(defmethod translate-branch-instruction
    ((instruction cleavir-ir:eq-instruction) inputs outputs successors)
  (cond ((nil-load-time-value (second inputs))
         (load-value (first inputs))
         (inst SYSTEM::JMPIF (second successors) nil)
         (inst SYSTEM::JMP (first successors)))
        (t
         (setup-funcall-args (list (first inputs)))
         (load-value (second inputs))
         (inst EQ)
         (inst SYSTEM::JMPIFNOT (second successors) nil)
         (inst SYSTEM::JMP (first successors))
         (incf *stack-pos*))))

(defmethod translate-branch-instruction
    ((instruction cleavir-ir:catch-instruction) inputs outputs successors)
  (declare (ignore inputs))
  (let ((const (make-constant-location (gensym "block"))))
    (inst SYSTEM::BLOCK-OPEN (constant-location-index const)
          (second successors))
    (inst SYSTEM::JMP (first successors))
    (setf (gethash (first outputs) *vars*)
          (make-instance 'block-location
                         :index (constant-location-index const)
                         :stack-index (1- *stack-pos*)))))

(defmethod translate-branch-instruction
    ((instruction cleavir-ir:unwind-instruction) inputs outputs successors)
  (inst SYSTEM::RETURN-FROM (- (block-location-index
                                (gethash (first inputs) *vars*))
                               *keywords*)))

(defmethod translate-branch-instruction
    ((instruction cleavir-ir:typeq-instruction) inputs outputs successors)
  (error "unimplemented")
  #+ (or)
  `(if (typep ,(first inputs) ',(cleavir-ir:value-type instruction))
       (go ,(first successors))
       (go ,(second successors))))

(defmethod translate-branch-instruction
    ((instruction cleavir-ir:fixnum-add-instruction) inputs outputs successors)
  (error "unimplemented")
  #+ (or)
  (let ((result (gensym)))
    `(let ((,result (+ ,(first inputs) ,(second inputs))))
       (cond ((typep result 'fixnum)
	      (setq ,(first outputs) ,result)
	      (go ,(first successors)))
	     ((plusp ,result)
	      (setq ,(first outputs)
		    (+ ,result (* 2 most-negative-fixnum)))
	      (go ,(second successors)))
	     (t
	      (setq ,(first outputs)
		    (- ,result (* 2 most-negative-fixnum)))
	      (go ,(second successors)))))))

(defmethod translate-branch-instruction
    ((instruction cleavir-ir:fixnum-sub-instruction) inputs outputs successors)
  (let ((result (gensym)))
    `(let ((,result (- ,(first inputs) ,(second inputs))))
       (cond ((typep result 'fixnum)
	      (setq ,(first outputs) ,result)
	      (go ,(first successors)))
	     ((plusp ,result)
	      (setq ,(first outputs)
		    (+ ,result (* 2 most-negative-fixnum)))
	      (go ,(second successors)))
	     (t
	      (setq ,(first outputs)
		    (- ,result (* 2 most-negative-fixnum)))
	      (go ,(second successors)))))))

(defmethod translate-branch-instruction
    ((instruction cleavir-ir:fixnum-less-instruction) inputs outputs successors)
  (declare (ignore outputs))
  `(if (< ,(first inputs) ,(second inputs))
       (go ,(first successors))
       (go ,(second successors))))

(defmethod translate-branch-instruction
    ((instruction cleavir-ir:fixnum-not-greater-instruction) inputs outputs successors)
  (declare (ignore outputs))
  `(if (<= ,(first inputs) ,(second inputs))
       (go ,(first successors))
       (go ,(second successors))))

(defmethod translate-branch-instruction
    ((instruction cleavir-ir:fixnum-equal-instruction) inputs outputs successors)
  (declare (ignore outputs))
  `(if (= ,(first inputs) ,(second inputs))
       (go ,(first successors))
       (go ,(second successors))))

(defmethod translate-branch-instruction
    ((instruction cleavir-ir:return-instruction) inputs outputs successors)
  (declare (ignore successors))
  (if (gethash (first inputs) *vars*)
      (load-value (first inputs))
      (warn "Merge point at RET."))
  (inst SYSTEM::SKIP (+ *nargs* (- *stack-pos*) 1))
  (inst SYSTEM::RET)
  #+ (or)
  `(return (values-list ,(first inputs))))

(defmethod translate-branch-instruction
    ((instruction cleavir-ir:unreachable-instruction) inputs outputs successors)
  (declare (ignore inputs outputs successors))
  `(error "Hit an UNREACHABLE-INSTRUCTION"))

;;; When the FUNCALL-INSTRUCTION is the last instruction of a basic
;;; block, it is because there is a call to a function that will never
;;; return, such as ERROR, and the instruction then has no successors
;;; (which is why it is at the end of the basic block).
;;;
;;; We therefore must provide a method on TRANSLATE-BRANCH-INSTRUCTION
;;; (in addition to the method on TRANSLATE-SIMPLE-INSTRUCTION)
;;; specialized to FUNCALL-INSTRUCTION.
(defmethod translate-branch-instruction
    ((instruction cleavir-ir:funcall-instruction) inputs outputs successors)
  (declare (ignore outputs successors))
  `(funcall ,(first inputs) ,@(rest inputs)))
